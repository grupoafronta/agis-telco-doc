
Opciones
========

EQ: Fabricantes
---------------

Con esta opción podemos añadir fabricantes para los modelos.

.. figure::  images/modulo/03_funcionalidades/image37.png
   :align:   center
   :width: 80%


EQ: Modelos
-----------

Con esta opción podemos añadir modelos para los equipos.

.. figure::  images/modulo/03_funcionalidades/image10.png
   :align:   center
   :width: 80%

Los modelos nos permiten añadir bandejas, divisores y paneles de parcheo.

.. figure::  images/modulo/03_funcionalidades/modelos_equipo.png
   :align:   center
   :width: 80%

Para que se añadan al crear o modificar un nuevo equipo, simplemente seleccionamos el fabricante y su modelo en los campos que corresponda.

SG: Código de color
-------------------

Esta opción nos permite crear un código de colores de un fabricante.

Para crear un nuevo cable le damos a añadir, esto permite crear tantos modelos como necesitemos. Para crear un código de colores, hay que tener claro una serie de campos:

.. figure::  images/modulo/03_funcionalidades/image149.png
   :align:   center
   :width: 80%

* NOMBRE DEL FABRICANTE:
    Identificador que, posteriormente, asignaremos al cable para la asignación de su código.
* FIBRAS POR TUBO:
    Módulo del cable, es decir, fibras por tubo, esto asignará tantos colores como fibras por tubo, que serán los mismos para cada conjunto de fibras por tubo.
* MAX TUBO:
    Número máximo de tubos que admite nuestro código de colores. Esto asignará tantos colores como tubos máximos tengamos en el cable.
* MODO 2 Colores:
    Permite que tanto tubos como fibras, admitan varios colores.

.. |image143| image:: images/modulo/03_funcionalidades/image143.png
    :width: 25

Una vez completamos los datos le damos al lápiz (|image143|) para configurar el código de colores.

La configuración de colores es sencilla, en columna acción, tan solo hay que pulsar el lápiz y entramos en el panel de colores.

.. figure::  images/modulo/03_funcionalidades/image152.png
   :align:   center
   :width: 80%

Por defecto, el valor es NULL, aunque aparezca en blanco, por lo que se  asigna colores pulsando sobre el círculo de la columna COLOR tanto para fibras como para tubos.

El icono de la brocha, sirve para copiar y pegar un color.
Brocha - Círculo a Copiar - Círculo a Pegar

Una vez asignados todos los colores, se puede proceder a asignarlos a un cable.

.. figure::  images/modulo/03_funcionalidades/image138.png
   :align:   center
   :width: 80%

Para asignar un color a un cable, se debe de seleccionar cable en cuestión, entrar al modo edición, y en el campo de Fabricante, se introducirá el nombre del fabricante que queramos asignar. Por ejemplo, Default

.. figure::  images/modulo/03_funcionalidades/image122.png
   :align:   center
   :width: 80%

Al dar a Guardar, y acceder a su Ventana de Información o ventana de información, ya se verá el código de colores, según lo que se haya asignado.

.. figure::  images/modulo/03_funcionalidades/image173.png
   :align:   center

La columna T representa el color del tubo, y la columna F el color de la fibra.

POP: Titular
------------

Con esta opción podemos añadir titulares para los pop.

.. figure::  images/modulo/03_funcionalidades/image142.png
   :align:   center
   :width: 80%




Opciones ADMIN - LÓGICA DE RED
*******************************

Propietarios
============

Al entrar dentro de la opción Propietarios, se puede editar los propietarios de los servicios ya creados o bien crear nuevos propietarios.

.. figure::  images/modulo/04_logica/image39.png
   :align:   center
   :width: 80%

Para crear nuevos Propietarios, se debe hacer clic sobre “Añadir” y rellenar los campos que se solicitan.

.. figure::  images/modulo/04_logica/image182.png
   :align:   center
   :width: 80%

   Formulario propietarios

Usuarios
========

En la pestaña de Usuarios, se indican todos los Usuarios que usan los servicios especificados.

.. figure::  images/modulo/04_logica/image20.png
   :align:   center
   :width: 80%

Para añadir nuevos usuarios, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan.

.. figure::  images/modulo/04_logica/image166.png
   :align:   center
   :width: 80%

   Formulario usuarios

Los usuarios registrados no tienen porque tener servicios asignados.

Contratos
=========

Al entrar dentro de Contratos, se pueden editar los contratos ya creados o bien crear nuevos contratos.

.. figure::  images/modulo/04_logica/image106.png
   :align:   center
   :width: 80%

Para añadir nuevos contratos se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan.

.. figure::  images/modulo/04_logica/image102.png
   :align:   center
   :width: 80%

   Formulario contratos

Equipo Terminal
===============

Al entrar dentro de Equipo Terminal, se pueden editar los equipos ya creados o bien crear nuevos equipos.

.. figure::  images/modulo/04_logica/image154.png
   :align:   center
   :width: 80%

Para añadir nuevos equipos terminal, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan.

.. figure::  images/modulo/04_logica/image177.png
   :align:   center
   :width: 80%

   Formulario equipo terminal





Capas de referencia
*******************
Mediante esta utilidad, podremos añadir capas de referencia a nuestro aGIS, es decir, capas que son dibujadas sobre el mapa para poder usarlas a modo de plantilla en nuestro nuevo diseño.

Podemos añadir capas de tipo punto, linea o polígono, pero nunca se podrán añadir capas que contengan más de un tipo.

Para añadir capas, accedemos a Capa de Referencia y nos aparecerá una lista con todas las que se encuentren en el proyecto.

.. figure::  images/modulo/05_utilidades/capas_referencia_1.png
   :align:   center
   :width: 80%


Añadiendo una nueva capa, debemos seleccionar el tipo de capa que vamos a subir, para añadir su cartoCSS, ponemos un nombre a la capa, elegimos el piso en el que se va a encontrar, editamos los estilos si lo deseamos y adjuntamos el archivo, debe ser un archivo GIS normalizado, preferiblemente GPKG, aunque soporta otros como kml, geojson, shp...

.. figure::  images/modulo/05_utilidades/capas_referencia_2.png
   :align:   center
   :width: 80%


Una vez completamos el formulario, ya podemos visualizar nuestra capa de referencia en el mapa

.. figure::  images/modulo/05_utilidades/capas_referencia_3.png
   :align:   center
   :width: 80%