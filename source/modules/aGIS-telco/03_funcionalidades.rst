########################
Funcionalidades del Mapa
########################

.. raw:: html

   <div style="text-align: justify;">
       <p>
           El acceso al mapa principal, que constituye el <strong>núcleo o core de la aplicación</strong>, es fundamental para el desarrollo de la mayoría de las actividades dentro de la plataforma. Desde este mapa, se facilita la digitalización precisa de cualquier elemento geográfico del entorno real, en forma de puntos, líneas o polígonos, permitiendo así una representación detallada de información geoespacial.
       </p>
       <p>
           El acceso al mapa es sencillo gracias al menú lateral, el cual permite navegar desde cualquier sección dentro de la aplicación, siguiendo la ruta:
       </p>
       <p>git git
           <strong>"VISUALIZACIÓN" → "Mapa"</strong>
       </p>
       <p>
           Una vez dentro, se puede observar que desde la opción <strong>"DISEÑO"</strong> se despliegan opciones adicionales, que se explorarán en mayor detalle más adelante.
       </p>
   </div>
   <br>


Elementos del Mapa
==================

.. raw:: html

   <div style="text-align: justify;">
       <p>
            En el <strong>Mapa</strong> se encuentran, a la izquierda y a la derecha, las siguientes funcionalidades, las cuales se detallan a continuación:
       </p>
   </div>


1. Zoom
--------

.. raw:: html

   <div style="text-align: justify;">
       <p>La funcionalidad de <strong>Zoom</strong> permite acercar o alejar la vista del mapa. También es posible realizar esta acción utilizando la rueda del ratón.</p>
   </div>


.. figure:: images/modulo/03_funcionalidades/image8.png
   :align: center

   Zoom


2. Pantalla Completa
--------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>Esta opción permite visualizar el mapa en modo de <strong>pantalla completa</strong>, proporcionando una experiencia de visualización ampliada.</p>
   </div>


.. figure::  images/modulo/03_funcionalidades/image165.png
   :align:   center

   Pantalla Completa


3. Deshacer/Avanzar Vista
--------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Esta herramienta registra el historial de la posición del cursor con cada clic de movimiento en el mapa, permitiendo tanto retroceder a vistas anteriores como avanzar hasta la última vista realizada.
       </p>
       <p>
           Además, dispone de un tercer botón <strong>(home)</strong>, que permite regresar a la vista general del mapa.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image212.png
   :align: center


4. Medir distancia
------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Esta herramienta facilita la medición precisa de distancias entre puntos en el mapa. Al activarla, es posible seleccionar puntos específicos mediante clics sucesivos, generando una medición acumulativa de distancia entre ellos.
       </p>
       <p>
           Para concluir la medición, basta con hacer doble clic, lo que restablece la herramienta y permite iniciar una nueva medición si se desea.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image111.png
   :align: center
   :width: 5%

   Herramienta de Medición de Distancias

.. figure:: images/modulo/03_funcionalidades/image41.png
   :align: center
   :width: 80%

   Ejemplo de Mediciones en el Mapa


5. Mapa Base
------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La plataforma ofrece una variedad de <strong>Mapas Base</strong> que sirven como referencia geográfica para el análisis y la visualización. Para seleccionar o cambiar el <strong>Mapa Base</strong>, se debe acceder a la opción correspondiente en el botón con forma de mapa del mundo.
       </p>
       <p>
           Actualmente, al realizar un cambio de <strong>Mapa Base</strong>, se reinician y eliminan automáticamente todas las capas no vectoriales, permitiendo cargar el nuevo mapa de manera óptima. Este proceso mejora la visualización general y ayuda a evitar problemas de carga y superposición.
       </p>
       <p>
           Al abrir el menú de <strong>Mapas Base</strong>, se despliega una ventana con todas las opciones disponibles, incluyendo una previsualización de cada mapa.
       </p>
   </div>


.. figure:: images/modulo/03_funcionalidades/image95.png
   :align: center

   Herramienta Mapa Base


.. figure:: images/modulo/03_funcionalidades/image23.png
   :align: center
   :width: 80%

   Catálogo de Mapas Base


.. raw:: html

   <div style="text-align: justify;">
       <p>
           La carga de <strong>Mapas Base</strong> en la plataforma es dinámica, lo que significa que la cantidad y el tipo de mapas disponibles pueden variar en función del proyecto o de la fecha. A continuación, se presentan algunos de los mapas por defecto disponibles:
       </p>
       <ul>
           <li><strong>ESRI Carreteras:</strong> Mapa público de ESRI que muestra la red de carreteras a nivel global.</li>
           <li><strong>Google Traffic:</strong> Mapa público de Google que detalla la red de carreteras a nivel global.</li>
           <li><strong>Google Satélite Hybrid:</strong> Ortofoto satelital pública de Google con etiquetado geográfico.</li>
           <li><strong>Google Satélite:</strong> Ortofoto satelital pública de Google sin etiquetado adicional.</li>
           <li><strong>Esri Satélite:</strong> Recurso público de ESRI que proporciona ortofotografía a nivel global.</li>
           <li><strong>CARTO Light:</strong> Mapa de CARTO diseñado para una visualización sencilla, con líneas minimalistas, ideal para una referencia básica.</li>
           <li><strong>IGN Catastro:</strong> Mapa catastral público del Instituto Geográfico Nacional (IGN), que muestra catastros nacionales.</li>
           <li><strong>IGN Catastro B/N:</strong> Versión en blanco y negro del mapa catastral público del IGN, centrada en catastros nacionales.</li>
           <li><strong>OSM:</strong> Mapa colaborativo y público de OpenStreetMap que proporciona información sobre calles y catastros. Muy recomendado para visualizaciones detalladas.</li>
       </ul>
   </div>


6. Selector de Nivel
---------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La herramienta <strong>Selector de Nivel</strong> permite navegar entre distintas plantas o niveles de un edificio
            o estructura representados en el mapa. Este gestor de alturas facilita la visualización y gestión de
            información geoespacial a distintas elevaciones, proporcionando una perspectiva completa de las instalaciones mapeadas.
       </p>
       <p>
           Para utilizar la herramienta, simplemente se debe hacer clic en la <strong>flecha hacia arriba</strong> para
            acceder a una planta superior, o en la <strong>flecha hacia abajo</strong> para descender a una planta inferior.
            El <strong>cuadro central</strong> indica el nivel actual, ayudando al usuario a ubicarse en la planta específica
            que está visualizando en cada momento.
       </p>
       <p>
           Esta funcionalidad es especialmente útil para mapas de interiores, como edificios de múltiples pisos, centros
            comerciales, complejos industriales, o cualquier otro entorno donde la visualización en diferentes alturas es
            crucial para un análisis detallado y preciso.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image48.png
   :align: center

   Herramienta Gestor de Plantas


7. URBA
-------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La herramienta <strong>URBA</strong> permite activar y desactivar los elementos URBA cuando están asignados al proyecto, facilitando el control de estas capas en el mapa. Su icono por defecto aparece en estado colapsado, manteniendo la interfaz ordenada y optimizada.
       </p>
       <p>
           Al posicionar el cursor sobre el icono, se desplegarán dos opciones de control. La primera opción permite <strong>activar o desactivar el mapa de URBA</strong>, mientras que la segunda habilita la <strong>activación y visualización de información de UUII por parcela</strong>. Esto significa que, además de mostrar las parcelas, se puede acceder a detalles específicos de cada UUII, proporcionando información valiosa para el análisis detallado de cada área. Ambas opciones están desactivadas de manera predeterminada, lo que permite al usuario activar solo las capas necesarias para su análisis.
       </p>
       <p>
           Esta herramienta es especialmente útil en proyectos de planificación urbana o en contextos donde es fundamental evaluar y representar distintas zonas y parcelas, permitiendo un control preciso sobre la información disponible en el entorno urbano.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image237.png
   :align: center

   Herramienta de Activación URBA

.. figure:: images/modulo/03_funcionalidades/image238.png
   :align: center
   :width: 80%

   Opciones de Control URBA y Parcelas UUII


.. raw:: html

   <div style="text-align: justify;">
       <p>
           La <strong>Herramienta de Medición</strong> permite al usuario no solo calcular distancias, sino también realizar mediciones avanzadas al dibujar un polígono en el mapa. Esta funcionalidad posibilita la identificación de intersecciones con parcelas y realiza un cálculo automatizado de las <strong>Unidades Urbanísticas Individuales (UUII)</strong> presentes en el área seleccionada.
       </p>
       <p>
           Para utilizar esta herramienta, el usuario puede delinear un polígono sobre la zona de interés, tras lo cual la plataforma calculará de forma automática las intersecciones con las parcelas correspondientes, proporcionando una visualización precisa de las UUII incluidas dentro del polígono. Además, se genera una suma total de las UUII en la zona delimitada, ofreciendo así un recurso clave para análisis espaciales detallados.
       </p>
       <p>
           Esta herramienta es especialmente útil en proyectos de desarrollo urbano o en estudios de impacto territorial, ya que facilita la cuantificación de recursos y la gestión de áreas específicas en función de los datos geoespaciales disponibles.
       </p>
   </div>


.. figure:: images/modulo/03_funcionalidades/image237b.png
   :align: center
   :width: 10%

   Medición de UUIs


.. raw:: html

   <div style="text-align: justify;">
       <p>
           La <strong>Herramienta de Medición</strong> permite al usuario no solo calcular distancias, sino también realizar mediciones avanzadas al dibujar un polígono en el mapa. Esta funcionalidad posibilita la identificación de intersecciones con parcelas y realiza un cálculo automatizado de las <strong>Unidades Urbanísticas Individuales (UUII)</strong> presentes en el área seleccionada.
       </p>
       <p>
           Para utilizar esta herramienta, el usuario puede delinear un polígono sobre la zona de interés, tras lo cual la plataforma calculará de forma automática las intersecciones con las parcelas correspondientes, proporcionando una visualización precisa de las UUII incluidas dentro del polígono. Además, se genera una suma total de las UUII en la zona delimitada, ofreciendo así un recurso clave para análisis espaciales detallados.
       </p>
       <p>
           Esta herramienta es especialmente útil en proyectos de desarrollo urbano o en estudios de impacto territorial, ya que facilita la cuantificación de recursos y la gestión de áreas específicas en función de los datos geoespaciales disponibles.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image237c.png
   :align: center
   :width: 80%

   Medición de UUIs


8. Mi Ubicación
---------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La funcionalidad de <strong>Mi Ubicación</strong> permite al usuario localizar su posición en el mapa de manera rápida y precisa. Al hacer clic en esta opción, la plataforma coloca un marcador sobre la ubicación actual del usuario y centra automáticamente el mapa en esa posición, mostrando sus <strong>coordenadas geográficas</strong>.
       </p>
       <p>
           Esta herramienta es ideal para orientarse en el entorno de la aplicación y facilita la navegación desde la ubicación actual. Además, al mostrar las coordenadas exactas, permite al usuario registrar o compartir su posición de manera eficiente.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image96.png
   :align: center
   :width: 80%

   Herramienta de Localización - Mi Ubicación


9. Copiar Coordenadas
----------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La funcionalidad de <strong>Copiar Coordenadas</strong>, ubicada en la parte superior derecha del mapa, permite al usuario seleccionar cualquier punto en el mapa y obtener sus <strong>coordenadas geográficas</strong> de manera precisa. Al hacer clic en el botón y luego seleccionar un punto en el mapa, las coordenadas se copian automáticamente al portapapeles, facilitando su uso en otros contextos.
       </p>
       <p>
           Además, esta herramienta permite alternar las unidades de medida: si se pulsa sobre el indicador de <strong>Latitud</strong>, se cambiarán las unidades según las preferencias del usuario. Esta flexibilidad en la elección de unidades facilita el trabajo con diferentes formatos de coordenadas en proyectos geoespaciales.
       </p>
       <p>
           La función <strong>Copiar Coordenadas</strong> es especialmente útil para quienes necesitan registrar ubicaciones precisas o compartir posiciones específicas de manera rápida y eficiente en motores externos a la aplicación. Cabe mencionar que la aplicación dispone de un método adicional para compartir coordenadas de manera interna, el cual se explicará a continuación.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image93.png
   :align: center
   :width: 60%

   Herramienta Copiar Coordenadas - Selección de Punto


.. figure:: images/modulo/03_funcionalidades/image108.png
   :align: center
   :width: 60%

   Alternar Unidades de Coordenadas



10. Buscador de Direcciones
---------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           El <strong>Buscador de Direcciones</strong>, ubicado en la interfaz de la aplicación, ofrece una funcionalidad avanzada de búsqueda de ubicaciones, similar al de Google Maps, pero con mejoras específicas para el análisis de cobertura. Este buscador permite al usuario ingresar una dirección y, tras la búsqueda, indica si la zona seleccionada se encuentra <strong>serviciable</strong> según la información de la capa <strong>cob_huellas</strong>.
       </p>
       <p>
           En caso de que la dirección no esté dentro de una zona serviciable, el sistema identificará y mostrará la <strong>zona más cercana que sí cuente con servicio</strong>. Esta funcionalidad facilita la planificación y el análisis de cobertura en áreas específicas, ofreciendo al usuario una herramienta eficiente para evaluar la disponibilidad de servicios en distintas ubicaciones.
       </p>
       <p>
           El <strong>Buscador de Direcciones</strong> es especialmente útil para quienes necesitan evaluar rápidamente la cobertura y planificar la expansión de servicios, proporcionando datos precisos y en tiempo real sobre la infraestructura disponible.
       </p>
   </div>


.. figure:: images/modulo/03_funcionalidades/image239.png
   :align: center
   :width: 30%

   Herramienta Buscador de Direcciones - Análisis de Cobertura


.. figure:: images/modulo/03_funcionalidades/image239b.png
   :align: center
   :width: 80%

   Herramienta Buscador de Direcciones - Detalle de consulta



11. Herramienta de Filtros de Capas
-----------------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La <strong>Herramienta de Filtros de Capas</strong> proporciona al usuario un control detallado sobre las capas de información visualizadas en el mapa. Esta herramienta permite activar o desactivar diversas capas, ya sea de forma completa o parcial (según las opciones disponibles en cada capa), facilitando una visualización personalizada y eficiente de los datos geoespaciales.
       </p>
       <p>
           Las capas se organizan en distintas categorías que abarcan múltiples aspectos del entorno geográfico y urbano. Entre las principales categorías disponibles se incluyen:
       </p>
       <ul>
           <li><strong>Red:</strong> Capas relacionadas con infraestructuras de red, tales como <em>gen_equipos</em> y <em>gen_segmentos</em>, donde es posible filtrar por atributos específicos como <strong>estado</strong>, <strong>modalidad</strong>, y <strong>jerarquía</strong>.</li>
           <li><strong>Civil:</strong> Capas vinculadas a la infraestructura civil, incluyendo <em>gen_canal</em> y <em>gen_struct</em>, que permiten visualizar detalles de tipo, estado y material de los elementos.</li>
           <li><strong>Administrativo:</strong> Capas que muestran información administrativa, como <em>gen_portaleros</em>, <em>rs_obs</em>, y <em>gen_zonas</em>, con opciones de filtro para tipo y estado de cada elemento.</li>
           <li><strong>SmartCity:</strong> Capas enfocadas en la infraestructura urbana y componentes de ciudades inteligentes, incluyendo <em>sc_area</em>, <em>sc_ruta</em>, y <em>sc_activo</em>.</li>
           <li><strong>Indoor:</strong> Capas que representan datos de interiores, tales como <em>rs_mob</em> y <em>rs_lin</em>, útiles para proyectos de cartografía de interiores o gestión de edificios.</li>
           <li><strong>Extras:</strong> Capas adicionales que proporcionan datos específicos, incluyendo <em>cob_huellas</em> para información de cobertura, <em>gen_suc</em>, <em>gen_medref</em>, y <em>gen_monitor</em>, cada una con atributos únicos para su filtrado.</li>
           <li><strong>Capas de Referencia:</strong> Capas adicionales que sirven de referencia contextual y se presentan al usuario cuando están disponibles en el proyecto.</li>
       </ul>
       <p>
           Al utilizar esta herramienta, el usuario tiene la capacidad de ajustar la visualización del mapa a sus necesidades específicas, activando únicamente las capas que sean relevantes para su análisis. En escenarios sin capas de referencia adicionales, el menú mostrará la opción de <strong>Capas de Referencia</strong> sin contenido disponible, manteniendo la estructura uniforme de la interfaz.
       </p>
       <p>
           La <strong>Herramienta de Filtros de Capas</strong> es especialmente útil para personalizar el entorno de trabajo y optimizar el flujo de datos en proyectos que requieren un enfoque detallado y específico en distintos aspectos geoespaciales y urbanos.
       </p>
   </div>


.. list-table::
   :widths: 50 50
   :align: center

   * - .. figure:: images/modulo/03_funcionalidades/filtros/filtro_telco.png
          :width: 90%

     - .. figure:: images/modulo/03_funcionalidades/filtros/filtro_zonas.png
          :width: 90%

.. raw:: html

   <div style="text-align: center; font-style: italic;">
       Filtros de Capas - Visualización de Filtro de Red y Filtro de Zonas
   </div>


12. Búsqueda de Red
-------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La funcionalidad de <strong>Búsqueda en Red</strong> permite localizar elementos específicos dentro de la red
            utilizando el <strong>campo nombre</strong> o, en caso de estar disponible, el <strong>campo UUID</strong>.
            Esta herramienta es particularmente útil en capas de puntos, líneas y polígonos, facilitando la identificación
            rápida y precisa de activos en el mapa.
       </p>
       <p>
           La herramienta presenta un menú de <strong>checkboxes</strong> que permite al usuario seleccionar las capas en
            las que desea realizar la búsqueda. Esta flexibilidad ofrece al usuario un control detallado sobre los resultados,
            permitiéndole enfocar la búsqueda en las capas más relevantes para su análisis.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image115.png
   :align: center
   :width: 30%
   :alt: Búsqueda de elementos en la red por nombre o UUID

   Búsqueda de elementos en la red por nombre o UUID

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La búsqueda genera una tabla con todos los resultados que coinciden con los criterios establecidos en la capa
            seleccionada. Al hacer clic en cualquiera de los elementos en la lista de resultados, la herramienta centra
            automáticamente el mapa en el elemento elegido y lo resalta visualmente, facilitando su localización inmediata
            en el entorno de trabajo.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image115b.png
   :align: center
   :width: 95%
   :alt: Resultado búsqueda de elementos en la red por nombre o UUID

   Resultado búsqueda de elementos en la red por nombre o UUID


13. Marcadores de Vista
-----------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La herramienta de <strong>Marcadores de Vista</strong> permite guardar vistas permanentes del mapa para futuras consultas, facilitando el acceso rápido a ubicaciones y configuraciones específicas en el entorno de trabajo.
       </p>
       <p>
           Su funcionamiento es intuitivo: al abrir su pestaña, se despliega una ventana donde aparecen las vistas previamente guardadas, junto con opciones para añadir nuevas vistas o eliminarlas según sea necesario. Esta característica permite al usuario crear un repositorio de ubicaciones clave para un acceso eficiente en cualquier momento.
       </p>
       <p>
           Cabe destacar que las vistas se almacenan a nivel de <strong>organización</strong>. Solo los usuarios que pertenecen a la misma organización pueden añadir, eliminar o consultar estos marcadores de vista, garantizando así una gestión centralizada y segura de las vistas compartidas dentro del equipo.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image213.png
   :align: center
   :alt: Ventana de vistas guardadas

   Ventana de vistas guardadas

.. figure:: images/modulo/03_funcionalidades/image214.png
   :align: center
   :alt: Opciones para añadir nuevas vistas

   Opciones para administrar vistas

.. figure:: images/modulo/03_funcionalidades/image215.png
   :align: center
   :alt: Opciones para borrar vistas

   Opciones para añadir vistas


14. Gestor de Impresión
-----------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La herramienta de <strong>Gestor de Impresión</strong>, ubicada en la parte superior derecha de la interfaz, permite acceder a diferentes opciones de impresión para capturar y documentar la visualización actual del mapa. Esta funcionalidad es especialmente útil para generar informes visuales o compartir información geoespacial de forma precisa y rápida.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image200.png
   :align: center
   :width: 30%
   :alt: Botón del Gestor de Impresión

   Botón del Gestor de Impresión

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Las opciones de impresión disponibles incluyen diferentes formatos de orientación, ofreciendo al usuario flexibilidad para elegir la disposición que mejor se adapte a sus necesidades:
       </p>
   </div>

.. |image201| image:: images/modulo/03_funcionalidades/image201.png
    :width: 25

.. |image202| image:: images/modulo/03_funcionalidades/image202.png
    :width: 25

.. |image203| image:: images/modulo/03_funcionalidades/image203.png
    :width: 25

|image201| Imprime lo que se visualiza en la pantalla.

|image202| Imprime lo que se visualiza en la pantalla en orientación horizontal.

|image203| Imprime lo que se visualiza en la pantalla en orientación vertical.


15. Vista en Calle (Street View)
---------------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La funcionalidad de <strong>Vista en Calle</strong> permite al usuario visualizar el entorno real del área central del mapa utilizando <strong>Google Street View</strong>. Al hacer clic en el icono correspondiente, la aplicación calcula el centro exacto del mapa actualmente renderizado y abre una nueva pestaña que muestra la vista en calle desde esa ubicación.
       </p>
       <p>
           Esta herramienta es particularmente útil para obtener una referencia visual de la ubicación en tiempo real, facilitando un análisis contextual del entorno urbano o rural directamente desde la interfaz de la aplicación.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image306.png
   :align: center
   :width: 10%
   :alt: Icono de Vista en Calle

   Icono de Vista en Calle (Street View)


16. Compartir Ubicación
------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La herramienta de <strong>Compartir Ubicación</strong> permite copiar las coordenadas seleccionadas en el mapa y almacenarlas en el portapapeles para su fácil compartición. Al hacer clic en el botón azul de <strong>Copiar Posición/Situar</strong>, las coordenadas elegidas se guardan y pueden ser enviadas a otro usuario de aGIS.
       </p>
       <p>
           El receptor de la ubicación puede pegar las coordenadas en el campo de entrada correspondiente y, al pulsar nuevamente el botón azul, el sistema centrará el mapa en la posición especificada, permitiendo una localización rápida y precisa en el entorno de trabajo.
       </p>
       <p>
           A diferencia de la función de búsqueda de elementos, esta herramienta permite identificar una zona específica en el mapa, independientemente de si existen elementos asociados en esa área, facilitando así la orientación y referencia geoespacial de cualquier ubicación.
       </p>
   </div>

.. figure:: images/modulo/03_funcionalidades/image307.png
   :align: center
   :width: 35%
   :alt: Herramienta Compartir Ubicación

   Herramienta Compartir Ubicación

17. Gestor de Modelos GM
-------------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           El <strong>Gestor de Modelos GM</strong> es una herramienta central de aGIS TELCO que permite al usuario diseñar y gestionar diversos elementos en el mapa, asignando campos y atributos específicos según el tipo de objeto a representar. La interfaz del gestor se organiza en varias categorías, cada una con diferentes elementos y configuraciones personalizables.
       </p>
       <p>
           La interfaz del gestor de modelos se divide en tres secciones:
       </p>
       <ul>
           <li><strong>Selector de elemento:</strong> permite elegir el tipo de elemento a dibujar en el mapa.</li>
           <li><strong>Datos generales:</strong> campos generales que varían según el tipo de elemento seleccionado, asegurando precisión en la representación.</li>
           <li><strong>Datos específicos:</strong> una sección opcional para el seguimiento temporal del elemento, permitiendo añadir observaciones y registrar fechas clave, tales como instalación, alta y baja.</li>
       </ul>
       <p>
           Algunos campos son comunes en todos los tipos de elementos, tales como:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Código identificativo único del elemento.</li>
           <li><strong>Nombre:</strong> Campo para asignar un nombre específico al elemento.</li>
           <li><strong>Estado:</strong> Estado actual del elemento (por ejemplo: Diseñado, Instalado, etc.).</li>
       </ul>
       <p>
           Una vez configurados los campos necesarios, se debe presionar el botón <strong>Dibujar</strong> para situar el objeto en el mapa. En el caso de capas lineales o poligonales, para finalizar el trazado, es necesario hacer doble clic en el último punto.
       </p>
       <p>
           Es posible dibujar tantos elementos como se desee hasta que se seleccione la opción de <strong>Terminar Edición</strong>.
            Todos los elementos creados adoptarán los atributos configurados, con excepción del campo <strong>Nombre</strong>,
            el cual se comportará según la configuración establecida en las <a href="02b_opciones_aGIS.html#opciones" target="_blank">opciones generales
            de aGIS</a>, en la pestaña <strong>Opciones</strong>. Esto proporciona flexibilidad para definir nombres automáticos o personalizados según
            las necesidades de cada proyecto.
       </p>
   </div>


1. Red
~~~~~~


.. raw:: html

   <div style="text-align: justify;">
       <h4>1.1. Equipos</h4>
   </div>


.. figure:: images/modulo/03_funcionalidades/image137.png
   :align: center
   :width: 30%
   :alt: Formulario de Equipos

   Formulario de Equipos


.. raw:: html

   <div style="text-align: justify;">
       <h5>Datos Generales:</h5>
       <ul>
           <li><strong>Mnemonico:</strong> Código identificativo del equipo.</li>
           <li><strong>Nombre:</strong> Campo opcional para asignar un nombre específico al equipo.</li>
           <li><strong>Estado:</strong> Define el estado actual del equipo.
               <ul>
                   <li>Proyectado</li>
                   <li>En construcción</li>
                   <li>Instalado</li>
               </ul>
           </li>
           <li><strong>Backhaul:</strong> Selección para activar o desactivar backhaul.</li>
           <li><strong>Tipo:</strong> Tipo de equipo específico.</li>
           <li><strong>Situación:</strong> Ubicación del equipo.</li>
           <li><strong>Fabricante:</strong> Nombre del fabricante.</li>
           <li><strong>Modelo:</strong> Especificación del modelo del equipo.</li>
       </ul>
   </div>

.. raw:: html

   <div style="text-align: justify;">
       <h4>1.2. Segmentos</h4>
   </div>

.. figure:: images/modulo/03_funcionalidades/image45.png
   :align: center
   :width: 30%
   :alt: Formulario de Segmentos

   Formulario de Segmentos

.. raw:: html

   <div style="text-align: justify;">
       <h5>Datos Generales:</h5>
       <ul>
           <li><strong>Nombre:</strong> Nombre del segmento o cable.</li>
           <li><strong>Tipo:</strong> Especifica el tipo de cable.
               <ul>
                   <li>Fibra G.655</li>
                   <li>Fibra G.652D</li>
                   <li>Fibra Mixto</li>
                   <li>Radio enlace</li>
                   <li>Coaxial</li>
                   <li>Alimentación</li>
                   <li>Tierra</li>
               </ul>
           </li>
           <li><strong>Capacidad:</strong> Número de fibras ópticas en el cable.</li>
           <li><strong>Situación:</strong> Medio en el que se encuentra el cable.
               <ul>
                   <li>Canalizado</li>
                   <li>Fachada</li>
                   <li>Interior</li>
                   <li>Poste</li>
                   <li>Zanja</li>
                   <li>Corrugado</li>
                   <li>Bandeja</li>
                   <li>Techo</li>
                   <li>Pared</li>
                   <li>Suelo</li>
                   <li>Vertical</li>
               </ul>
           </li>
           <li><strong>Cubierta:</strong> Tipo de cubierta del cable.
               <ul>
                   <li>PESP</li>
                   <li>KT</li>
                   <li>TKT</li>
                   <li>PKP</li>
                   <li>PKCP</li>
               </ul>
           </li>
           <li><strong>Propietario:</strong> Dueño del cable.</li>
           <li><strong>Estado:</strong> Estado actual de la instalación.
               <ul>
                   <li>Proyectado</li>
                   <li>En construcción</li>
                   <li>Instalado</li>
               </ul>
           </li>
       </ul>
   </div>


2. Civil
~~~~~~~~~~


.. raw:: html

   <div style="text-align: justify;">
       <p>
           El apartado <strong>Civil</strong> está dedicado a la gestión de infraestructuras relacionadas con la obra civil en la red. Esta sección permite detallar dos tipos principales de elementos:
           <strong>Estructuras</strong> y <strong>Canalizaciones</strong>.
       </p>
       <p>
           Las <strong>Estructuras</strong> se representan en el mapa como puntos y suelen incluir elementos tales como arquetas, cámaras y otros componentes estáticos que sirven de soporte o conexión en la red. Cada estructura puede configurarse con atributos específicos como tipo, material, función y dimensiones, lo que facilita una descripción completa de su propósito y características.
       </p>
       <p>
           Las <strong>Canalizaciones</strong> se representan en el mapa como segmentos lineales y describen los conductos o trayectorias subterráneas, aéreas o superficiales que conectan diferentes estructuras de la red. Las canalizaciones pueden configurarse para reflejar detalles como longitud, tipo de instalación (por ejemplo, canalización o paso aéreo), y el número de obturadores, brindando una visión precisa de la infraestructura de la red.
       </p>
       <p>
           Además, el sistema permite especificar los <strong>conductos y subconductos</strong> dentro de cada canalización, proporcionando un nivel de detalle adicional en la configuración. Este detalle permite asociar los segmentos canalizados con los respectivos subconductos, permitiendo así visualizar en un esquema de sección la ubicación exacta de cada segmento dentro de la estructura de subconductos. De esta forma, se obtiene una representación precisa de cómo se distribuyen y organizan los elementos dentro de la red de canalización.
       </p>
       <p>
           Para asociar un subconducto a un conducto específico, primero es necesario crear el conducto. Una vez creado, se debe seleccionar el conducto, el cual se marcará en color <strong>amarillo</strong>, indicando que está activo para asociar subconductos. A continuación, los subconductos pueden ser definidos y asociados al conducto activo, asegurando una estructura organizada y jerárquica dentro de la canalización.
       </p>
   </div>


.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/image_civil.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Estructuras


     - .. figure:: images/modulo/03_funcionalidades/image_civilb.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Canalizaciones

   * - .. figure:: images/modulo/03_funcionalidades/image_civilc.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Estructuras


     - .. figure:: images/modulo/03_funcionalidades/image_civild.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Canalizaciones



.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Formulario de Civil
   </div>


3. Admin
~~~~~~~~~~~~~~

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La pestaña <strong>Admin</strong> en el <strong>Gestor de Modelos GM</strong> permite la gestión de elementos administrativos clave dentro de la red. A través de esta pestaña, es posible definir y configurar <strong>POPs</strong>, <strong>Zonas</strong>, <strong>Portaleros</strong> y <strong>Observaciones</strong>, proporcionando una estructura organizada que facilita el mantenimiento y la supervisión de la infraestructura de red.
       </p>
       <p>
           Cada sección cuenta con opciones específicas y permite asignar atributos detallados que aseguran una documentación precisa de la red. A continuación, se describen las características de cada sección.
       </p>

       <h5>POPs</h5>
       <p>
           Los <strong>POPs</strong> representan puntos de presencia o nodos críticos dentro de la red, tales como nodos de distribución, cabeceras de red o centros de telecomunicaciones. Estos elementos permiten la gestión de atributos relevantes como el tipo de POP, su estado, y detalles de titularidad y dirección.
       </p>

       <h5>Zonas</h5>
       <p>
           La sección de <strong>Zonas</strong> está diseñada para definir áreas de influencia específicas dentro de la red, tales como zonas de despliegue de redes de fibra o áreas de cobertura de nodos y centros de datos.
           Es altamente recomendable digitalizar las áreas de influencia de <strong>CTOs</strong> (Cajas Terminales Ópticas), <strong>CDs</strong> (Cajas de Distribución) y zonas de despliegue. Esta digitalización permite, entre otras funcionalidades, lo siguiente:
       </p>
       <ul>
           <li>Generar códigos mnemónicos automáticos para una mejor identificación y organización de las zonas, municipios, cabeceras.</li>
           <li>Cargar automáticamente los portaleros (entradas de edificios o puntos de acceso) que se encuentren dentro de la zona definida, facilitando la planificación y gestión del despliegue.</li>
           <li>Obtener los datos necesarios para presentar informes requeridos por entidades gubernamentales. En particular, la digitalización de zonas facilita la preparación del informe anual de Consulta Pública sobre la Identificación de las “Zonas Blancas NGA” y “Zonas Grises NGA”, en cumplimiento con las normativas del Ministerio.</li>
       </ul>
       <p>
           Este nivel de detalle en la documentación de zonas asegura un control preciso y eficiente del despliegue de red, optimizando tanto los recursos como el cumplimiento de requisitos regulatorios.
       </p>

       <h5>Portaleros</h5>
       <p>
           Los <strong>Portaleros</strong> permiten la especificación y localización de puntos de acceso o entradas de edificios dentro de las áreas de cobertura. Estos elementos ayudan en la planificación y documentación de la infraestructura de red, permitiendo definir atributos como tipo de estructura, dirección, y estado.
       </p>

       <h5>Observaciones</h5>
       <p>
           Las <strong>Observaciones</strong> ofrecen un espacio para registrar comentarios o anotaciones adicionales sobre la infraestructura de la red. Esta funcionalidad permite realizar un seguimiento detallado de actividades específicas y mantener una documentación completa, incluyendo tipos de observaciones como certificaciones, instalaciones, obras civiles, y notas internas.
       </p>
   </div>



.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/admin.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - POPs


     - .. figure:: images/modulo/03_funcionalidades/adminb.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Zonas

   * - .. figure:: images/modulo/03_funcionalidades/adminc.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Portaleros


     - .. figure:: images/modulo/03_funcionalidades/admind.png
          :align: center
          :width: 100%
          :alt: Formulario de Civil - Obs

.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Formulario de Admin
   </div>


4. SmartCity
~~~~~~~~~~~~~~~~~


.. raw:: html

   <div style="text-align: justify;">
       <p>
           La pestaña <strong>SmartCity</strong> está destinada a gestionar y documentar elementos relacionados con ciudades inteligentes. Esta sección permite configurar y visualizar diferentes tipos de infraestructura urbana y servicios, clasificándolos en tres categorías: <strong>Activo</strong>, <strong>Ruta</strong> y <strong>Area</strong>. Cada categoría proporciona opciones específicas para un seguimiento preciso y detallado de los elementos urbanos.
       </p>

       <h5>Activo</h5>
       <p>
           La subpestaña <strong>Activo</strong> permite gestionar elementos urbanos individuales como monumentos, farolas y otros activos de la ciudad. Los campos disponibles incluyen:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Identificador único del activo.</li>
           <li><strong>Nombre:</strong> Campo para asignar un nombre específico al activo.</li>
           <li><strong>Estado:</strong> Estado actual del activo, seleccionable entre opciones como:
               <ul>
                   <li>Diseñado</li>
                   <li>Instalado</li>
               </ul>
           </li>
           <li><strong>Tipo:</strong> Clasificación del tipo de activo, incluyendo opciones como:
               <ul>
                   <li>Monumento</li>
                   <li>Farola</li>
               </ul>
           </li>
           <li><strong>Horario Apertura:</strong> Horario en el que el activo está disponible o en funcionamiento.</li>
           <li><strong>Horario Cierre:</strong> Hora de cierre o finalización de disponibilidad del activo.</li>
           <li><strong>Arte:</strong> Campo para especificar detalles artísticos del activo, si aplica (por ejemplo, para un monumento).</li>
           <li><strong>Fecha Último Pintado:</strong> Fecha en la que el activo fue pintado por última vez (aplicable a farolas y otros activos de mantenimiento regular).</li>
           <li><strong>Material:</strong> Material de construcción del activo.</li>
           <li><strong>Base:</strong> Tipo de base o soporte del activo.</li>
       </ul>

       <h5>Ruta</h5>
       <p>
           La subpestaña <strong>Ruta</strong> permite definir y configurar líneas de transporte urbano, como líneas de autobús, proporcionando datos clave sobre el recorrido y la dirección. Los campos disponibles incluyen:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Código identificativo de la ruta.</li>
           <li><strong>Nombre:</strong> Campo opcional para asignar un nombre específico a la ruta.</li>
           <li><strong>Estado:</strong> Define el estado actual de la ruta.
               <ul>
                   <li>Diseñado</li>
                   <li>Instalado</li>
               </ul>
           </li>
           <li><strong>Tipo:</strong> Tipo de ruta, con opciones como:
               <ul>
                   <li>Línea de Autobús</li>
               </ul>
           </li>
           <li><strong>Línea:</strong> Nombre o número de la línea de autobús.</li>
           <li><strong>Dirección:</strong> Dirección de inicio o finalización de la ruta.</li>
           <li><strong>CP:</strong> Código postal asociado a la ruta.</li>
       </ul>

       <h5>Area</h5>
       <p>
           La subpestaña <strong>Area</strong> permite gestionar áreas de interés o zonas específicas de la ciudad que requieren seguimiento o control. Esto incluye espacios públicos o zonas de influencia de servicios. Los campos disponibles incluyen:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Código único del área.</li>
           <li><strong>Nombre:</strong> Nombre asignado al área.</li>
           <li><strong>Estado:</strong> Estado del área, seleccionable entre:
               <ul>
                   <li>Diseñado</li>
                   <li>Instalado</li>
               </ul>
           </li>
           <li><strong>Tipo:</strong> Tipo de área o zona (por ejemplo, parque, zona comercial).</li>
           <li><strong>Capacidad:</strong> Capacidad del área, en función de su uso o propósito.</li>
           <li><strong>Material:</strong> Material principal del área.</li>
       </ul>
   </div>


.. list-table::
   :widths: 33 33 34
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/sc_activo.png
          :align: center
          :width: 100%
          :alt: Formulario de SmartCity - Activo


     - .. figure:: images/modulo/03_funcionalidades/sc_ruta.png
          :align: center
          :width: 100%
          :alt: Formulario de SmartCity - Ruta


     - .. figure:: images/modulo/03_funcionalidades/sc_area.png
          :align: center
          :width: 100%
          :alt: Formulario de SmartCity - Área


.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Formulario de SmartCity
   </div>


5. Indoor
~~~~~~~~~~~~~~


.. raw:: html

   <div style="text-align: justify;">
       <p>
           La pestaña <strong>Indoor</strong> en el <strong>Gestor de Modelos GM</strong> permite gestionar elementos de infraestructura interna, especialmente útil para representaciones detalladas de edificios y mobiliario dentro de edificaciones. Esta sección se divide en dos pestañas: <strong>Mobiliario</strong> y <strong>Planta</strong>, proporcionando una capacidad avanzada para gestionar y documentar la infraestructura interna en detalle.
       </p>

       <h5>Mobiliario</h5>
       <p>
           La pestaña <strong>Mobiliario</strong> permite definir los elementos internos de una edificación con una variedad de opciones de tipo, tales como:
       </p>
       <ul>
           <li><strong>Bastidor</strong></li>
           <li><strong>Equipo Eléctrico</strong> (E. Fuerza)</li>
           <li><strong>Luminaria</strong></li>
           <li><strong>Aire Acondicionado</strong> (AACC)</li>
           <li><strong>Módulo de Sistema</strong> (M. Sistema)</li>
           <li><strong>Módulo de Radio</strong> (M. Radio)</li>
           <li><strong>Transmisión</strong></li>
           <li><strong>Antena</strong></li>
           <li><strong>Torre</strong></li>
           <li><strong>Mástil</strong></li>
           <li><strong>Otro</strong></li>
       </ul>
       <p>
           Cada tipo de elemento puede configurarse con atributos específicos, tales como propietario, capacidad, y coordenadas tridimensionales (X, Y, Z) para una ubicación precisa dentro del edificio.
       </p>

       <h5>Planta</h5>
       <p>
           La pestaña <strong>Planta</strong> permite crear una capa geométrica y lineal que representa edificaciones completas o partes específicas de la infraestructura interna. Aunque no se indican tipos específicos en esta pestaña, su objetivo es ofrecer una representación detallada de los niveles internos de un edificio. Esta funcionalidad es esencial para documentar la disposición física de las plantas de una edificación y para organizar visualmente la infraestructura que se encuentra dentro de cada nivel.
       </p>
   </div>


.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/indoor_mobiliario.png
          :align: center
          :width: 100%
          :alt: Formulario de Indoor - Mobiliario


     - .. figure:: images/modulo/03_funcionalidades/indoor_planta.png
          :align: center
          :width: 100%
          :alt: Formulario de Indoor - Planta


.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Formulario de Indoor
   </div>
   <br>
   <br>

6. Extras
~~~~~~~~~~~~~~

.. raw:: html

   <div style="text-align: justify;">
       <p>
           La pestaña <strong>Extras</strong> en el <strong>Gestor de Modelos GM</strong> permite gestionar elementos adicionales de la red, específicamente en lo relacionado con la cobertura y las unidades ópticas de red (<strong>ONTs</strong>). A través de esta pestaña, es posible definir detalles sobre áreas de cobertura y las configuraciones específicas de ONTs, brindando un control preciso sobre los aspectos de accesibilidad de la red.
       </p>

       <h5>Cobertura</h5>
       <p>
           La subpestaña <strong>Cobertura</strong> permite definir áreas de cobertura dentro de la red. Aquí se pueden asignar varios atributos, entre ellos:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Código identificativo de la cobertura.</li>
           <li><strong>Nombre:</strong> Campo para asignar un nombre específico a la cobertura.</li>
           <li><strong>Estado:</strong> Define el estado actual de la cobertura (por ejemplo: Diseñado).</li>
           <li><strong>Tipo:</strong> Tipo de red dentro de la cobertura, con opciones tales como:
               <ul>
                   <li>WIFI</li>
                   <li>WIMAX</li>
                   <li>FTTH</li>
                   <li>HFC</li>
               </ul>
           </li>
           <li><strong>Zona:</strong> Clasificación de la cobertura, que puede ser:
               <ul>
                   <li>Real</li>
                   <li>Prevista</li>
               </ul>
           </li>
           <li><strong>Descripción:</strong> Campo de texto para agregar información adicional sobre la cobertura.</li>
           <li><strong>UI Viviendas:</strong> Cantidad de unidades inmobiliarias residenciales en el área.</li>
           <li><strong>UI Locales:</strong> Cantidad de unidades inmobiliarias comerciales en el área.</li>
           <li><strong>Serviciable:</strong> Indicador de si la zona es accesible para los servicios de la red.</li>
           <li><strong>Fecha Prevista:</strong> Fecha esperada para que la zona esté operativa.</li>
       </ul>
       <p>
           La digitalización de una cobertura permite que el sistema integre dicha información con el <strong>Buscador de Direcciones</strong> (<a href="#buscador-de-direcciones" title="Ir a la sección Buscador de Direcciones">Funcionalidad 10</a>), lo cual facilita la localización de zonas específicas de cobertura a través de búsquedas en el mapa. Esta funcionalidad mejorada permite que el buscador lea las zonas de cobertura definidas, brindando una experiencia de usuario más informativa y precisa.
       </p>

       <h5>ONTs</h5>
       <p>
           La subpestaña <strong>ONTs</strong> permite la gestión de unidades ópticas de red, facilitando la configuración y el seguimiento de estos dispositivos clave. Las opciones disponibles incluyen:
       </p>
       <ul>
           <li><strong>Mnemonico:</strong> Código identificativo de la ONT.</li>
           <li><strong>Nombre:</strong> Campo opcional para asignar un nombre específico a la ONT.</li>
           <li><strong>Estado:</strong> Define el estado actual de la ONT (Diseñado, Instalado, etc.).</li>
           <li><strong>Servidor:</strong> Asignación de servidor para la ONT en cuestión.</li>
           <li><strong>SN ONT:</strong> Número de serie de la ONT para identificación única.</li>
       </ul>
   </div>


.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/extras_cobertura.png
          :align: center
          :width: 100%
          :alt: Formulario de Extras - Cobertura


     - .. figure:: images/modulo/03_funcionalidades/extras_ont.png
          :align: center
          :width: 100%
          :alt: Formulario de Extras - ONT


.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Formulario de Extras
   </div>
   <br>
   <br>


18. Tabla de Atributos
-------------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La <strong>tabla de atributos</strong> en <strong>aGIS</strong> dispone de un selector que permite al usuario mostrar los datos correspondientes a una capa específica. Al seleccionar una capa en el selector, la tabla despliega automáticamente los atributos de dicha capa. Además, al hacer clic en cualquier fila de la tabla, <strong>aGIS</strong> centra y hace zoom en el elemento seleccionado, funcionando de manera similar al buscador de elementos.
        </p>
        <p>
            La tabla de atributos también permite la <strong>descarga en formato CSV o Excel</strong> de los datos visibles en pantalla. Para una mayor personalización de la visualización, el usuario puede cambiar la paginación entre 10, 20, 50 y 100 líneas. Además, la tabla incluye un buscador que permite localizar cualquier elemento por nombre o UUID en la capa seleccionada en el selector.
        </p>
        <p>
            Es importante señalar que la tabla de atributos muestra los datos almacenados en la base de datos (BBDD). Como tal, algunos valores pueden no ser inmediatamente legibles para el usuario, pero están optimizados para almacenamiento y gestión técnica. Sin embargo, al hacer clic en la opción <strong>Traducir Campos</strong>, el sistema traduce estos valores de acuerdo con el modelo de datos de la herramienta, facilitando su interpretación para el usuario.
        </p>
    </div>



.. figure::  images/modulo/03_funcionalidades/tablaAtributos_icon.png
   :align:   center
   :width: 5%

   Herramienta Tabla Atributos


.. figure::  images/modulo/03_funcionalidades/tablaAtributos.png
   :align:   center
   :width: 100%

   Tabla de Atributos


.. raw:: html

   <div style="text-align: justify;">
        <p>
            En el panel de la <strong>Tabla de Atributos</strong> de aGIS TELCO, se ofrecen diversas opciones de edición y exportación para gestionar los datos de las capas. A continuación se detallan las funcionalidades de cada botón:
        </p>

        <ul>
            <li><img alt="Restaurar datos iniciales" src="../../_images/image206.png" style="width: 20px;">
                <strong>Restaurar datos iniciales:</strong> Este botón permite volver a la configuración inicial de los datos, descartando cualquier cambio realizado en la sesión actual.</li>

            <li><img alt="Guardar cambios" src="../../_images/image207.png" style="width: 20px;">
                <strong>Guardar cambios:</strong> Al hacer clic en este botón, se guardan permanentemente todos los cambios aplicados en los datos de la capa activa.</li>

            <li><img alt="Borrar el elemento seleccionado" src="../../_images/image205.png" style="width: 20px;">
                <strong>Borrar el elemento seleccionado:</strong> Permite eliminar de forma definitiva el elemento actualmente seleccionado en la capa.</li>

            <li><img alt="Consulta SQL" src="../../_images/tablaAtributos_sql.png" style="width: 20px;">
                <strong>Restaurar datos iniciales SQL:</strong> Permite realizar consultas a la BBDD.</li>

            <li><img alt="Exportar a CSV" src="../../_images/image208.png" style="width: 20px;">
                <strong>Exportar a .CSV:</strong> Exporta la información de la capa actual en formato CSV, ideal para manipulación en hojas de cálculo.</li>

            <li><img alt="Exportar a SHP" src="../../_images/image209.png" style="width: 20px;">
                <strong>Exportar a .SHP:</strong> Genera un archivo SHP compatible con la mayoría de los sistemas GIS para un intercambio de datos eficiente.</li>

            <li><img alt="Exportar a KML" src="../../_images/image210.png" style="width: 20px;">
                <strong>Exportar a .KML:</strong> Exporta los datos en formato KML, adecuado para visualización en plataformas como Google Earth.</li>

            <li><img alt="Exportar a GPKG" src="../../_images/image211.png" style="width: 20px;">
                <strong>Exportar a .GPKG:</strong> Guarda la capa en un archivo GeoPackage, un formato geoespacial que admite múltiples capas vectoriales en un solo archivo.</li>
        </ul>
    </div>


.. figure::  images/modulo/03_funcionalidades/consultaSQL_TA.png
   :align:   center
   :width: 100%

   Consulta SQL de Tabla de Atributos


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para <strong>desactivar la traducción de elementos</strong> y gestionar los datos directamente desde la base de datos, se debe activar el modo de <strong>Consulta SQL</strong> al hacer clic en el icono correspondiente de
            <img alt="Consulta SQL" src="../../_images/tablaAtributos_sql.png" style="width: 20px;">. Esta función habilita la posibilidad de realizar consultas directas a la base de datos <strong>PostgreSQL</strong>,
            permitiendo a usuarios con conocimientos avanzados en SQL y GIS interactuar con los datos de manera más detallada y precisa.
        </p>
        <p>
            Al activar esta opción de consulta, el sistema mostrará los datos <strong>tal cual están almacenados en la base de datos</strong>, sin aplicar traducciones de campos. Esto asegura que el usuario visualice e interactúe directamente con los valores exactos de la base de datos, lo cual es particularmente útil para realizar análisis específicos o ajustes detallados en los datos.
        </p>
        <p>
            Además, el resultado de la consulta puede <strong>exportarse en varios formatos</strong>, como <strong>CSV</strong>, <strong>KML</strong> y <strong>GPKG</strong>, proporcionando flexibilidad para compartir y analizar los datos fuera del sistema. Esta funcionalidad avanzada brinda una mayor profundidad y precisión en la gestión de la información almacenada en <strong>aGIS</strong>, adaptándose a las necesidades de aquellos usuarios que requieran un control y una visibilidad exhaustiva de los datos brutos en su entorno de trabajo.
        </p>
    </div>


19. Gestor de Conexiones
------------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El <strong>Gestor de Conexiones</strong> permite digitalizar las <strong>fusiones o conexiones</strong> entre distintos elementos de la red en <strong>aGIS</strong>. Con esta herramienta, los usuarios pueden conectar <strong>cable con cable</strong>, <strong>cable con equipo</strong>, <strong>equipo con cable</strong>,y <strong>equipo con equipo</strong>, facilitando la gestión y organización de las conexiones en el sistema.
        </p>
        <p>
            El funcionamiento del Gestor de Conexiones es intuitivo. Al abrir la herramienta, se <strong>marca en amarillo el elemento origen</strong>. El usuario debe hacer clic en el elemento que desea designar como origen. Una vez seleccionado, el sistema mostrará los distintos <strong>terminales de salida</strong> de los subelementos (si el origen es un equipo) o los <strong>subsegmentos</strong> en caso de ser un cable.
        </p>
        <p>
            A continuación, se define el <strong>destino</strong> seleccionando el elemento de destino que se desea conectar. En este punto, el sistema mostrará los <strong>terminales de entrada</strong> correspondientes al elemento de destino.
        </p>
        <p>
            Para realizar la conexión, simplemente se debe seleccionar los terminales que se desean conectar. Para una selección múltiple, se puede mantener presionada la tecla <strong>CTRL</strong> al hacer clic en los elementos, o <strong>SHIFT</strong> si son consecutivos. Una vez seleccionados, se debe hacer clic en el <strong>botón verde</strong> para conectar. En caso de querer desconectar, se puede utilizar el <strong>botón rojo</strong>.
        </p>
        <p>
            En la parte superior, el <strong>botón azul de intercambio</strong> permite realizar un <strong>swap</strong> o intercambio entre origen y destino, lo cual agiliza el proceso de conexión al permitir al usuario alternar rápidamente entre ambos elementos.
        </p>
        <p>
            El Gestor de Conexiones incluye diversas <strong>comprobaciones de integridad</strong> para asegurar que las conexiones sean correctas. Por ejemplo, se requiere que el número de terminales seleccionados en el origen y el destino sea el mismo, evitando así la posibilidad de conectar un solo terminal de salida a varios de entrada de forma simultánea. Estas verificaciones garantizan que las conexiones se realicen de manera lógica y consistente dentro de la red.
        </p>
    </div>


.. list-table::
   :widths: 33 33 34
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/gestor_conexiones.png
          :align: center
          :width: 100%
          :alt: Gestor de Conexiones


     - .. figure:: images/modulo/03_funcionalidades/gestor_conexiones_a.png
          :align: center
          :width: 100%
          :alt: Gestor de Conexiones - Elemento A


     - .. figure:: images/modulo/03_funcionalidades/gestor_conexiones_b.png
          :align: center
          :width: 100%
          :alt: Gestor de Conexiones - Elemento B


.. raw:: html

   <div style="text-align: center; font-style: italic; margin-top: 10px;">
       Gestor de Conexiones
   </div>
   <br>
   <br>


19. Gestor de Disponibilidad
----------------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta de <strong>Gestor de Disponibilidad</strong> permite modificar la <strong>disponibilidad</strong> y la <strong>modalidad</strong> de las fibras en un segmento específico.
            Al activar esta herramienta, se despliega una ventana en el lado derecho de la pantalla que solicita al usuario seleccionar un segmento en el mapa.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image17.png
   :align: center
   :width: 50%

   Selector de Segmento para Gestión de Disponibilidad

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Una vez seleccionado el cable, se cargarán las fibras asociadas. Dependiendo del botón que se pulse, la herramienta mostrará la información de <strong>disponibilidad</strong> o de <strong>modalidad</strong>.
        </p>
        <p>
            Al seleccionar la opción de <strong>disponibilidad</strong>, las fibras disponibles se indican en verde, mientras que las no disponibles aparecen en rojo. Para cambiar el estado de disponibilidad de una o varias fibras, simplemente se deben seleccionar las fibras deseadas y pulsar la opción correspondiente.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image133.png
   :align: center
   :width: 50%

   Vista de Disponibilidad de Fibras

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En el caso de seleccionar la opción de <strong>modalidad</strong>, el proceso es similar al de disponibilidad, pero muestra las distintas <strong>modalidades</strong> de las fibras en lugar de su disponibilidad.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image71.png
   :align: center
   :width: 50%

   Vista de Modalidad de Fibras


20. Gestor de Asociaciones
--------------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta de <strong>Gestor de Asociaciones</strong> permite atribuir <strong>segmentos</strong> a <strong>canalizaciones</strong>, facilitando así la gestión de la infraestructura subterránea y aérea.
        </p>
        <p>
            Para asociar un segmento a una canalización, primero se debe abrir la herramienta y seleccionar el <strong>segmento</strong> que se desea asociar. Luego, se selecciona la <strong>canalización</strong> deseada en el mapa. A continuación, se debe elegir el <strong>conducto</strong> específico por el que se desea que pase el segmento, y finalmente pulsar el botón verde para aplicar la asociación o el botón rojo para eliminarla.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image174.png
   :align: center
   :width: 50%

   Gestor de Asociaciones de Segmentos y Canalizaciones

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Esta información de asociación se puede verificar nuevamente en la pestaña <strong>Esquema</strong> de la canalización seleccionada, donde, al igual que en el gestor de asociaciones, se mostrará un esquema con los conductos en blanco por donde pasa cada cable.
        </p>
    </div>


21. Check Inteligente de Red
----------------------------

.. figure:: images/modulo/03_funcionalidades/checkInteligente.png
   :align: center
   :width: 5%

   Herramienta Check Inteligente de Red.

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al hacer clic en el ícono de <strong>Checkeo Inteligente de Red</strong>, se despliega una nueva ventana que
            muestra una tabla con todos aquellos segmentos que están <strong>canalizados</strong> y en estado
            <strong>serviciable</strong>, pero que <strong>no están asociados</strong> a ninguna <strong>canalización</strong>.
            Esto significa que dichos segmentos no tienen un SUC (Sistema de Utilización de Conducción) asignado.
        </p>
        <p>
            Esta funcionalidad permite un control efectivo de los <strong>SUCs</strong> que se han gestionado en los
            elementos canalizados, proporcionando información detallada sobre cada segmento que cumple con los criterios
            especificados.
        </p>
        <p>
            La tabla incluye columnas como <strong>UUID</strong> (identificador único del segmento),
            <strong>Nombre</strong> del segmento, <strong>Usuario</strong> que realizó la última modificación,
            y el campo <strong>TT</strong> (Tiempo de la última modificación). Esta información es útil para realizar
            un seguimiento detallado de la administración de cada segmento. Al igual que en la Tabla de Atributos, al
            clickar en cualquier fila de la tabla, aGIS centrará y remarcará en el mapa el elemento seleccionado.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/checkInteligenteTabla.png
   :align: center
   :width: 95%

   Resultados Check Inteligente de Red.


22. Escala
----------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Esta herramienta se encuentra situada en la esquina inferior izquierda de la pantalla y muestra una <strong>escala</strong> que se ajusta dinámicamente en función de la ampliación o reducción del mapa. Esta escala facilita al usuario la interpretación de las distancias en el contexto del mapa visualizado.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image116.png
   :align: center
   :width: 10%

   Escala


23. Leyenda
-----------


.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta de <strong>Leyenda</strong> se encuentra ubicada en la esquina inferior derecha de la pantalla y muestra la <strong>simbología</strong> utilizada en el mapa. Al hacer clic en la leyenda, se despliegan las diferentes <strong>capas</strong> junto con los <strong>símbolos</strong> que tienen asignados. Estos símbolos ayudan a interpretar visualmente los elementos representados en cada capa.
        </p>
        <p>
            Para una referencia completa de todos los símbolos, puede consultarse el apartado de <strong>Simbología</strong> al final del manual, concretamente en el <strong>Capítulo 7</strong>.
        </p>
    </div>


.. figure::  images/modulo/03_funcionalidades/image31.png
   :align:   center

   Leyenda



Edición
=======

Opciones de Edición en Mapa
---------------------------


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al seleccionar un elemento previamente digitalizado en el mapa, se despliega su <strong>ventana de información</strong>, la cual será detallada en el siguiente subapartado. Además, al hacerlo, se habilitan una serie de botones específicos cuya funcionalidad varía en función del tipo de elemento seleccionado, permitiendo acciones personalizadas según el contexto del objeto.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/botonesEdicion.png
   :align: center
   :width: 10%

   Herramientas Edición.


.. raw:: html

    <div style="text-align: justify;">
        <p>
            A continuación, se describe la funcionalidad de cada uno de los botones de edición disponibles en la interfaz:
        </p>
        <ul>
            <li><strong>Botón de Girar:</strong> Permite rotar el elemento seleccionado en el mapa. Esta función está disponible únicamente para elementos de tipo <strong>línea</strong> o <strong>polígono</strong>, y no se aplica a elementos de tipo <strong>punto</strong>.</li>
            <li><strong>Botón de Selección:</strong> Habilita la selección de elementos individuales en el mapa, permitiendo editar su geometría en el caso de líneas o polígonos. Además, al seleccionar un elemento, se abre una <strong>Ventana de Información</strong> que permite editar sus parámetros como si se estuviese creando en el <strong>Gestor de Modelos</strong>.</li>
            <li><strong>Botón de Movimiento:</strong> Facilita el desplazamiento de elementos seleccionados a una nueva ubicación dentro del mapa, útil para realizar ajustes en la posición de los elementos.</li>
            <li><strong>Botón de Corte:</strong> Permite dividir o segmentar elementos, como líneas o polígonos, facilitando una edición precisa de las geometrías representadas en el mapa.</li>
            <li><strong>Botón de Eliminación:</strong> Permite borrar elementos seleccionados del mapa, removiéndolos permanentemente de la visualización y, si está configurado, también de la base de datos.</li>
        </ul>
    </div>
    <br>
    <br>


Opciones de Edición en Menú
---------------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Estas herramientas de edición están disponibles de manera constante al entrar en el <strong>mapa</strong> de la aplicación, independientemente del elemento que se haya clicado en el mapa. De esta forma, el usuario puede acceder fácilmente a las funciones de rotación, selección, movimiento, corte y eliminación en cualquier momento, sin necesidad de seleccionar previamente un objeto específico en el mapa.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/edicion_menu.png
   :align: center
   :width: 30%

   Herramientas Edición en Menú Principal



Gestor de Canalizaciones
~~~~~~~~~~~~~~~~~~~~~~~~


.. raw:: html

    <div style="text-align: justify;">
        <h5>Gestor de Canalizaciones</h5>
        <p>
            La herramienta <strong>Gestor de Canalizaciones</strong> permite asignar <strong>segmentos</strong> a <strong>canalizaciones</strong>, lo cual facilita la gestión de la infraestructura subterránea o aérea dentro del sistema.
        </p>
        <p>
            Para realizar la asignación, primero abre la herramienta y selecciona el <strong>segmento</strong> que deseas vincular, seguido de la <strong>canalización</strong> correspondiente. A continuación, selecciona el <strong>conducto</strong> específico por el que debe pasar el segmento y, finalmente, pulsa el botón verde para aplicar la asociación o el botón rojo para eliminarla.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image174.png
   :align: center
   :width: 50%
   :alt: Interfaz del Gestor de Canalizaciones

   Interfaz del Gestor de Canalizaciones para asignar segmentos a canalizaciones



Gestor de Asociaciones
~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

    <div style="text-align: justify;">
        <h5>Gestor de Asociaciones</h5>
        <p>
            La herramienta <strong>Gestor de Asociaciones</strong> permite realizar asociaciones entre distintos elementos del sistema, independientemente de la <strong>capa</strong> en la que se encuentren. Esta funcionalidad facilita la vinculación y gestión de relaciones entre diversos tipos de elementos dentro de la infraestructura.
        </p>
        <p>
            Para utilizar esta herramienta, abre el <strong>Gestor de Asociaciones</strong>, selecciona el primer elemento que deseas asociar y, a continuación, selecciona el segundo elemento al cual deseas vincularlo. Tras realizar la selección, pulsa el botón verde para aplicar la asociación o el botón rojo para eliminar la asociación existente.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image_associations.png
   :align: center
   :width: 50%
   :alt: Interfaz del Gestor de Asociaciones

   Interfaz del Gestor de Asociaciones para vincular elementos de distintas capas



Herramientas Avanzadas
~~~~~~~~~~~~~~~~~~~~~~


.. figure:: images/modulo/03_funcionalidades/herramientas_avanzadas.png
   :align: center
   :width: 30%
   :alt: Herramientas Avanzadas

   Herramientas Avanzadas

.. raw:: html

    <div style="text-align: justify;">
        <h5>Cortar segmentos y canalizaciones</h5>
        <p>
            Para cortar un <strong>segmento</strong> o una <strong>canalización</strong>, se debe utilizar esta herramienta. A continuación, se explica el proceso con un segmento como ejemplo. Simplemente, pulsa el botón de cortar y selecciona dos puntos en el segmento: el primero con un clic y el segundo con un doble clic.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image131.png
   :align: center
   :width: 80%
   :alt: Herramienta de corte de segmentos

   Herramienta de corte de segmentos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Tras realizar el corte, se obtienen dos cables que mantienen el mismo nombre y las mismas características, aunque las conexiones se mantienen en los extremos originales del segmento. Este comportamiento se aprecia mejor en un ejemplo concreto.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image83.png
   :align: center
   :width: 50%
   :alt: Conexión en el extremo izquierdo del cable

   Conexión en el extremo izquierdo del cable

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Después del corte, la parte izquierda del cable permanece conectada como se muestra en la captura. En caso de que estuviera ocupada también en el lado derecho, las conexiones pasarían a estado no conectado. La parte derecha del cable, al no tener conexiones en el extremo derecho, se presenta de la siguiente manera.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image52.png
   :align: center
   :width: 50%
   :alt: Conexión en el extremo derecho del cable después del corte

   Conexión en el extremo derecho del cable después del corte

.. raw:: html

    <div style="text-align: justify;">
        <h5>Copiar item</h5>
        <p>
            Esta herramienta permite copiar ítems que son puntuales, como equipos, cocas y estructuras. Una vez seleccionada la herramienta, simplemente haz clic sobre el ítem que deseas copiar y podrás pegarlo las veces que quieras, volviendo a hacer clic en el mapa para cada copia.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image88.png
   :align: center
   :width: 80%
   :alt: Herramienta de copiado de ítems puntuales

   Herramienta de copiado de ítems puntuales





Ventanas de Información
=======================

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Las <strong>ventanas de información</strong> se despliegan en la parte derecha de la pantalla cuando se hace clic en los distintos elementos representados en el mapa. Estas ventanas presentan un desglose detallado de la información relevante sobre el objeto seleccionado, adaptándose en función del tipo de elemento y de los servicios disponibles en el sistema.
        </p>
        <p>
            La funcionalidad de las ventanas de información permite al usuario realizar diversas acciones, tales como finalizar la configuración de los elementos, visualizar sus esquemas, acceder a documentos relacionados, supervisar el estado del trabajo, revisar incidencias, modificar los datos introducidos en el momento de la creación del objeto y consultar un registro de las modificaciones realizadas.
        </p>
        <p>
            En función del tipo de elemento seleccionado, las <strong>ventanas de información</strong> muestran exclusivamente la información relevante para ese objeto, omitiendo datos que no sean aplicables. Esta personalización mejora la experiencia del usuario al enfocarse en los aspectos específicos y necesarios de cada elemento.
        </p>
        <p>
            Además, al activar una ventana de información, se habilita automáticamente una serie de <strong>botones</strong> situados justo debajo de las funcionalidades en el lado derecho de la pantalla. Estos botones también se adaptan según el elemento seleccionado, proporcionando herramientas específicas que permiten una gestión integral y eficiente de cada objeto en el mapa.
        </p>
    </div>



.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/infowindows_cto.png
          :align: center
          :width: 100%
          :alt: Ventana de Información - CTOs


     - .. figure:: images/modulo/03_funcionalidades/infowindows_segmento.png
          :align: center
          :width: 100%
          :alt: Ventana de Información - Segmentos


.. raw:: html

   <div style="text-align: center; font-style: italic;">
       Ventanas de Información
   </div>

.. raw:: html

    <div style="text-align: justify;">
        <br>
        <br>
        <p>
            Las ventanas de información, aunque presentan diferencias según el tipo de elemento, comparten ciertas
            similitudes en su estructura y funcionalidades. En la parte superior, encontramos una serie de <strong>iconos</strong>
            que permiten <strong>exportar</strong> la información, visualizar el <strong>timestamp</strong> de las modificaciones
            realizadas al elemento, <strong>minimizar</strong> la Ventana de Información o <strong>cerrarla</strong>.
        </p>
    </div>

Croquis General
---------------

.. raw:: html

    <div style="text-align: justify;">

        <p>
            A continuación, se muestran los <strong>datos generales</strong> del elemento, que incluyen sus características
            específicas. En el caso de una <strong>CTO</strong>, se presenta además un <strong>croquis general</strong>
            donde se visualizan los subelementos del equipo y sus conexiones con segmentos cercanos.
        </p>
        <p>
            Por otro lado, la Ventana de Información de <strong>segmentos</strong> muestra los <strong>subsegmentos</strong> (fibras),
            donde se puede observar el código de colores utilizado y el estado de las conexiones, representadas mediante
            flechas. Además, al hacer clic en los puntos situados a cada lado de cada fibra, es posible <strong>registrar
            una medida reflectométrica</strong> o calcular las <strong>pérdidas teóricas</strong> en función de los valores
            introducidos Opciones Admin.
        </p>
        <p>
            La Ventana de Información de segmentos también permite realizar un <strong>análisis de lógica</strong>, el cual verifica
            si la fibra del segmento está conectada a un <strong>puerto PON</strong> activo de la OLT. Si este es el caso,
            la conexión se marcará en rojo y se especificará el puerto correspondiente. Esta funcionalidad de análisis se
            configura en el módulo de <strong>Lógica de Red</strong>.
        </p>
    </div>


.. list-table::
   :widths: 50 50
   :header-rows: 0

   * - .. figure:: images/modulo/03_funcionalidades/croquisGeneral_equipo.png
          :align: center
          :width: 100%
          :alt: Ventana de Información - Croquis General Equipos


     - .. figure:: images/modulo/03_funcionalidades/lógicaRed_segmentos.png
          :align: center
          :width: 100%
          :alt: Ventana de Información - Lógica de Red Segmentos


.. raw:: html

   <div style="text-align: center; font-style: italic;">
       Ventanas de Información - Croquis General y Lógica de Red en Segmentos.
   </div>
   <br>
   <br>


Cables en Paso
---------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En el apartado <strong>Cables en Paso</strong>, es posible visualizar las fibras de los cables que no están conectadas a una entrada del equipo, siempre y cuando haya al menos una fibra del cable que sí esté conectada al equipo. En caso contrario, el cable no aparecerá representado en esta sección.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/cables_paso.png
   :align: center
   :width: 50%

   Cables en Paso


Elementos del Equipo
--------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En <strong>Elementos del Equipo</strong> se representan los subelementos digitalizados dentro del elemento
            principal, en este caso, el equipo. Dependiendo del tipo de subelemento, se muestra de una forma específica,
            indicando siempre su <strong>grado de ocupación</strong> en paneles o el estado de las conexiones en el caso
            de bandejas o splitters.
        </p>
        <p>
            Además, en bandejas y splitters, están disponibles los puntos redondos (similares a los del croquis de la
            Ventana de Información de segmentos) que permiten calcular las <strong>pérdidas teóricas</strong> y registrar
            <strong>medidas reflectométricas (SOR)</strong>.
        </p>
    </div>

.. list-table::
   :widths: 50 50
   :align: center

   * - .. figure:: images/modulo/03_funcionalidades/elementosEquipo.png
          :width: 100%

     - .. figure:: images/modulo/03_funcionalidades/elementosEquipob.png
          :width: 100%

.. raw:: html

   <div style="text-align: center; font-style: italic;">
       Ventanas de Información - Elementos del Equipo
   </div>
   <br>
   <br>


Gestión de Abonados
--------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Esta sección permite visualizar qué <strong>abonados</strong> están digitalizados en la <strong>CTO</strong> seleccionada. Además, ofrece la posibilidad de ver la ubicación del abonado, editar o eliminar su información, y, por supuesto, añadir nuevos abonados.
        </p>
        <p>
            Las funciones de <strong>añadir</strong> y <strong>editar</strong> abonados se explicarán en detalle en el apartado <strong>Utilidades, Gestor de Abonados</strong>.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/cto_gestorAbonados.png
   :align: center
   :width: 70%

   Ventanas de Información - Gestor de Abonados.


Datos Específicos
------------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Este apartado es común a todas las <strong>Ventana de Informacións</strong> y permite rellenar los campos específicos que se muestran, además de añadir cualquier tipo de observación. Las observaciones ingresadas serán también renderizadas en el nombre de la CTO para facilitar su identificación.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/datosEspecificos.png
   :align: center
   :width: 70%

   Ventanas de Información - Datos Específicos.



Asociaciones
------------


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Las <strong>asociaciones</strong> permite visualizar las asociaciones de un elemento con otro, sin importar la capa en la que se
            encuentren. Esta funcionalidad es clave para integrar y relacionar elementos diversos en el sistema,
            facilitando la gestión y visualización de conexiones complejas entre distintos componentes de la red.
        </p>
    </div>



.. figure:: images/modulo/03_funcionalidades/infowindows_asociaciones.png
   :align: center
   :width: 70%

   Ventanas de Información - Asociaciones


Gestor Documental
-----------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El <strong>Gestor Documental</strong> permite visualizar la documentación asociada al elemento mostrado en la
            <strong>Ventana de Información</strong>. Esta documentación, subida en el Gestor Documental de Utilidades, puede
            visualizarse desde la misma <strong>Ventana de Información</strong>. Esta funcionalidad es clave para integrar
            y relacionar elementos diversos en el sistema, facilitando la gestión y visualización de conexiones complejas
            entre distintos componentes de la red.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/infowindows_gestorDocumental.png
   :align: center
   :width: 70%

   Ventanas de Información - Gestor Documental




Funcionalidades Extras
=======================

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Existen <strong>funciones extras</strong> disponibles que pueden ser accesadas desde las distintas ventanas
            mencionadas anteriormente, especialmente desde las <strong>ventanas de información</strong>. Estas funciones
            adicionales permiten una mayor interactividad y opciones de gestión directa sobre los elementos seleccionados,
            brindando al usuario herramientas avanzadas para personalizar y ajustar la configuración de cada elemento en el sistema.
        </p>
    </div>


Propagación
-----------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En resumen, la <strong>propagación</strong> permite resaltar el <strong>camino lógico de continuidad</strong> de una fibra, tanto en el esquema de una bandeja como en el de una fibra. Para visualizar este camino, simplemente haz clic sobre la fibra o el terminal en uno de los esquemas, y el sistema mostrará el recorrido completo de la señal a través de la infraestructura conectada.
        </p>
    </div>

.. figure::  images/modulo/03_funcionalidades/image129.png
   :align:   center
   :width: 100%

   Ejemplo de Propagación de una fibra.


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Como se observa en la imagen, el <strong>recorrido de la propagación</strong> se muestra sombreado en amarillo después de seleccionar la <strong>posición 1</strong> del segmento. Esta visualización permite identificar fácilmente el camino lógico de continuidad a lo largo de la infraestructura.
        </p>
        <p>
            En la siguiente imagen, se aprecia un resultado similar al hacer clic en una de las fibras que conectan a los terminales en el <strong>Croquis General</strong> de un equipo. Esta función facilita el seguimiento y análisis del trayecto de la señal entre los distintos elementos de la red.
        </p>
    </div>



.. figure::  images/modulo/03_funcionalidades/image235.png
   :align:   center
   :width: 60%

   Croquis General - Selección de fibra conectada.


.. figure::  images/modulo/03_funcionalidades/image234.png
   :align:   center
   :width: 100%

   Propagación - Seleccionando la fibra conectada a los elementos.


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Independientemente de la forma en la que se ha consultado la <strong>propagación</strong>, se habilita un nuevo <strong>submenú</strong> que permite revisar la <strong>continuidad</strong> elemento por elemento. Esta opción facilita un análisis detallado del trayecto de la señal, permitiendo explorar cada conexión en la ruta de propagación.
        </p>
    </div>


.. figure::  images/modulo/03_funcionalidades/image236.png
   :align:   center
   :width: 45%

   Análisis de Circuito Físico


.. |analisisContinuidad| image:: images/modulo/03_funcionalidades/analisisContinuidad.png
       :width: 80

.. raw:: html

    <div style="text-align: justify;">
        <p>
            aGIS realiza la <strong>sumatoria de la longitud horizontal</strong> de la red en función de los elementos por los que el <strong>haz de luz</strong> se ha propagado, desde el último elemento hasta su cabecera. Es importante tener en cuenta que, si se selecciona un elemento intermedio en la propagación, el sistema también mostrará la propagación completa.
        </p>
        <p>
            La <strong>longitud real</strong> es igual a la horizontal salvo en los casos donde el usuario haya especificado, como en el caso de los <strong>Asbuilt</strong>, la verdadera longitud en campo.
        </p>
        <p>
            Por último, el sistema dispone del botón de <a class="reference internal" href="../../_images/analisisContinuidad.png"><img alt="analisisContinuidad" src="../../_images/analisisContinuidad.png" style="width: 100px;"></a>, el cual mostrará en una tabla –exportable en formatos <strong>CSV</strong> o <strong>Excel</strong>– todos los elementos por los que se ha propagado el circuito físico. Al igual que en otras tablas vistas previamente, al hacer clic en cualquier fila, el sistema centra y resalta el elemento seleccionado en el mapa.
        </p>
    </div>


.. figure::  images/modulo/03_funcionalidades/analisisContinuidadTabla.png
   :align:   center
   :width: 100%

   Tabla de Análisis de Continuidad


.. raw:: html

    <div style="text-align: justify;">
        <p>
            En la columna <strong>Orden</strong>, los valores negativos indican elementos <strong>aguas arriba</strong>
            en las conexiones (es decir, elementos padres), mientras que los valores positivos representan elementos
            <strong>aguas abajo</strong> (o elementos hijos). El valor cero corresponde al elemento en el que se ha
            hecho clic para consultar su propagación.
        </p>
        <p>
            En caso de que el haz de luz pase por un <strong>equipo</strong>, se muestra el <strong>subequipo</strong>
            correspondiente en la tabla de propagación. En la columna <strong>Fibra Pos</strong>, se indica la fibra del
            segmento por la que se propaga el circuito; si el trayecto pasa a través de subequipos, se especifica el
            <strong>terminal</strong> de cada subequipo que interviene en la propagación además de otros datos relevantes.
        </p>
    </div>
    <br>
    <br>


Registros de Medidas SOR
------------------------

.. figure::  images/modulo/03_funcionalidades/image229.png
   :align:   center
   :width: 50%

   Registro de Medidas SOR

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El <strong>registro de medidas</strong> siempre está relacionado con los <strong>subequipos</strong> y/o los <strong>subsegmentos</strong>. Después de explicar los distintos campos de la capa, se detalla el proceso para crear registros de medida.
        </p>
        <p>
            Como encabezado de la <strong>Ventana de Información del Gestor de Medidas</strong>, se muestran los datos más relevantes, como la <strong>Potencia Transmitida</strong> y la <strong>Potencia Recibida</strong>.
        </p>
        <h5>Datos Generales</h5>
        <ul>
            <li><strong>Nombre: Archivo de Medida</strong></li>
            <li><strong>Fecha Medida:</strong> Fecha en la que se realiza la medición.</li>
            <li><strong>Potencia Tx (dBm):</strong> Valor de la potencia Transmitida en dBm.</li>
            <li><strong>Potencia Rx (dBm):</strong> Valor de la potencia Recibida en dBm.</li>
            <li><strong>Lambda (nm):</strong> Longitud de onda de la medida, por defecto 1350 nm.</li>
            <li><strong>Longitud (m):</strong> Longitud de la medida en metros.</li>
        </ul>
        <p>
            Como en las demás capas, esta capa también cuenta con un apartado para <strong>Datos Específicos</strong>.
        </p>
        <br>
        <h5>Equipos: Bandejas</h5>
        <p>
            Para registrar o crear una medida en un subequipo, como en una <strong>bandeja</strong>, es necesario acceder a la bandeja en cuestión. Si el permiso está activado, aparecerá un icono de <strong>+</strong> para añadir una medida, como se muestra en la imagen. Al hacer clic en el icono, se abre una ventana para ingresar los datos del registro de medida.
        </p>
        <p>
            Por supuesto, cabe la posibilidad de subir dicha medida asociada al elemento utilizando el <strong>Gestor Documental</strong> antes mencionado.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image226.png
   :align: center
   :width: 50%
   :alt: Registro de Medida en una Bandeja

   Registro de Medida en una Bandeja

.. raw:: html

    <div style="text-align: justify;">
        <h5>Equipos: Splitters</h5>
        <p>
            Los <strong>splitters</strong> funcionan de la misma manera que las bandejas. Para registrar una medida, simplemente accede al splitter deseado y utiliza el icono de <strong>+</strong> si el permiso está habilitado.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image227.png
   :align: center
   :width: 50%
   :alt: Registro de Medida en un Splitter

   Registro de Medida en un Splitter

.. raw:: html

    <div style="text-align: justify;">
        <h5>Segmentos: Fibras</h5>
        <p>
            En el caso de los <strong>segmentos</strong>, se debe acceder a la <strong>Ventana de Información</strong> del segmento y luego al <strong>Esquema de Segmento</strong>. En esta vista, aparecerá el icono de <strong>+</strong> que permite registrar la medida en la fibra seleccionada.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image228.png
   :align: center
   :width: 50%
   :alt: Registro de Medida en una Fibra de Segmento

   Registro de Medida en una Fibra de Segmento

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Las tres opciones de registro de medidas están disponibles para cada terminal o subsegmento, tanto en su <strong>entrada</strong> como en su <strong>salida</strong>. Dependiendo de la selección de entrada o salida, la medida se mostrará a la derecha o izquierda en la interfaz.
        </p>
        <p>
            La ventana de registro de datos que se abre para completar la medida contiene los mismos campos que los presentes en la Ventana de Información.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/image224.png
   :align: center
   :width: 50%
   :alt: Ventana de Registro de Medida

   Ventana de Registro de Medida

.. figure:: images/modulo/03_funcionalidades/image225.png
   :align: center
   :width: 50%
   :alt: Ventana de Registro de Medida Adicional

   Ventana de Registro de Medida Adicional


Pérdidas Teóricas
-----------------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al igual que para registrar una medida, siguiendo los mismos pasos, los usuarios también tienen disponible una <strong>estimación teórica de las pérdidas</strong> en la <strong>segunda</strong> y <strong>tercera ventana</strong>.
        </p>
        <p>
            La tabla de pérdidas teóricas se divide en tres secciones: pérdidas a la <strong>izquierda</strong> del elemento seleccionado, pérdidas a la <strong>derecha</strong> del mismo, y el <strong>resultado total</strong>, que muestra la sumatoria desde la cabecera hasta el último elemento de la red.
        </p>
        <p>
            En el caso de que se muestre una columna en rojo, como se observa en el ejemplo, esto indica que a partir de ese elemento la señal experimentará una <strong>multipropagación</strong> debido a la presencia de un splitter en el camino.
        </p>
        <p>
            Para realizar estos cálculos teóricos de pérdidas, el <strong>usuario administrador</strong> de la organización ha configurado valores por defecto en la sección <strong>Utilidades -> M. Reflectométricas -> Pérdidas Teóricas</strong> del menú de la izquierda.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/perdida_teorica.png
   :align: center
   :width: 50%
   :alt: Pérdida Teórica

   Calculadora de Pérdida Teórica


Ayuda OTDR
----------


.. raw:: html

    <div style="text-align: justify;">
        <p>
            La opción de <strong>Ayuda OTDR</strong> es una herramienta de asistencia disponible exclusivamente cuando
            se está visualizando el mapa. Es accesible desde el <strong>menú de la izquierda</strong> en la sección de
            <strong>Utilidades</strong>, dentro del submenú de <strong>M. Reflectométricas</strong>.
            Cabe destacar que el menú de Utilidades será explicado en su apartado correspondiente.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/boton_ayudaOTDR.png
   :align: center
   :width: 30%
   :alt: Botón de Ayuda OTDR

   Botón de Ayuda OTDR disponible en el menú de Utilidades dentro de M. Reflectométricas


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al seleccionar la opción de <strong>Ayuda OTDR</strong>, se habilita la posibilidad de realizar una estimación
            aproximada del lugar donde podría estar produciéndose una <strong>pérdida de potencia</strong> en la fibra.
            Es decir, introduciendo un valor en metros y seleccionando el elemento en el cual se ha realizado la medición,
            la herramienta calculará y mostrará en el mapa una estimación del posible lugar del error en campo de todos los
            segmentos conectados, facilitando la identificación y localización del problema.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/resultado_ayudaOTDR.png
   :align: center
   :width: 100%
   :alt: Botón de Ayuda OTDR

   Resultado de Ayuda OTDR.



Mnemonizador
------------

.. |mnemonizador_boton| image:: images/modulo/03_funcionalidades/mnemonizador_boton.png
       :width: 60



.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta <strong>Mnemonizador</strong> permite asignar un <strong>código mnemónico</strong> a una zona
            específica dentro del proyecto. Esta funcionalidad es especialmente útil en <strong>aGIS Telco</strong>,
            ya que permite el diseño de múltiples cabeceras por proyecto, lo que facilita la identificación de cabeceras,
            municipios y otras áreas de interés mediante códigos mnemónicos.
        </p>
        <p>
            Para seleccionar una zona y aplicar el mnemonizador, es necesario utilizar alguna de las zonas previamente
            digitalizadas, como la <strong>zona de influencia (CTO)</strong>, la <strong>zona de una CD</strong> o una
            <strong>zona de despliegue</strong>. Esta herramienta permite establecer un código mnemónico que facilita
            la gestión y organización de las diferentes áreas dentro del proyecto.
        </p>
        <p>
            En el proceso de uso del mnemonizador, se debe hacer clic en una de estas zonas o áreas previamente digitalizadas.
            En el siguiente ejemplo, se muestra cómo aplicar el mnemonizador para todo un municipio utilizando la
            <strong>zona de despliegue</strong>.
            En la Ventana de Información, tenemos el icono del
            <a class="reference internal" href="../../_images/mnemonizador_boton.png"><img alt="mnemonizador_boton" src="../../_images/mnemonizador_boton.png" style="width: 30px;"></a>
            mnemonizador en la parte superior derecha.
        </p>
    </div>


.. figure:: images/modulo/03_funcionalidades/mnemonizador.png
   :align: center
   :width: 50%
   :alt: Interfaz de Mnemonizador

   Interfaz de selección de capas para mnemonización dentro del área seleccionada


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al hacer clic en el icono de <strong>Mnemonizador</strong>, se abre una ventana que permite seleccionar las <strong>capas</strong> que se desean mnemonizar, aplicándose únicamente a los elementos dentro de la <strong>intersección del área seleccionada</strong>.
        </p>
        <p>
            En esta interfaz, se muestra el <strong>total de elementos</strong> en cada capa y, entre corchetes, el número de elementos pendientes de mnemonizar, es decir, aquellos elementos digitalizados después de la primera mnemonización. Esto permite llevar un control de los elementos que aún no tienen un código mnemónico asignado.
        </p>
        <p>
            El <strong>código de mnemonización</strong> utilizado será el previamente asignado en la zona seleccionada para mnemonizar, asegurando consistencia en la identificación de las áreas. Además, existe la opción de <strong>sobrescribir mnemónicos existentes</strong> para actualizar los códigos mnemónicos si es necesario.
        </p>
    </div>


Portaleros
----------


.. |portaleros_boton| image:: images/modulo/03_funcionalidades/portaleros_boton.png
       :width: 60


.. raw:: html

    <div style="text-align: justify;">
        <p>
            La funcionalidad de <strong>Portaleros</strong> permite obtener información detallada sobre los portales y
            numeración de edificios en la zona marcada. Para acceder a esta funcionalidad, se sigue el mismo procedimiento
            que el <strong>Mnemonizador</strong>: selecciona una zona previamente digitalizada, como una zona de
            influencia (CTO), una zona de una CD o una zona de despliegue, y luego accede al ícono
            <a class="reference internal" href="../../_images/portaleros_boton.png"><img alt="portaleros_boton" src="../../_images/portaleros_boton.png" style="width: 30px;"></a>
            en la <strong>Ventana de Información</strong> ubicada en la parte superior derecha.
        </p>
        <p>
            Esta herramienta está conectada con el servicio <strong>CartoCiudad</strong>, lo que permite acceder a una
            base de datos completa y actualizada de portaleros en la zona seleccionada. Esto resulta útil para obtener
            información precisa sobre la disposición y numeración de edificios, facilitando el diseño y planificación
            de despliegues de red en áreas urbanas.
        </p>
    </div>

.. figure:: images/modulo/03_funcionalidades/portaleros.png
   :align: center
   :width: 100%
   :alt: Interfaz de la Funcionalidad de Portaleros

   Interfaz de la Funcionalidad de Portaleros, obteniendo datos desde CartoCiudad


.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta <strong>Portaleros</strong> proporciona una tabla detallada con todos los portaleros interceptados en la zona seleccionada. Esta tabla permite seleccionar individualmente los portaleros deseados, seleccionar todos, o deseleccionar según sea necesario. También se incluye una función de búsqueda para localizar un portalero específico de forma rápida y eficiente.
        </p>
        <p>
            En la parte inferior de la herramienta, se dispone de una opción de <strong>Duplicar</strong> mediante un checkbox, útil en casos donde se requiere añadir un portalero que ya ha sido cargado previamente.
        </p>
        <p>
            Al utilizar esta herramienta, los portaleros seleccionados se cargan en la capa <strong>gen_portaleros</strong>, convirtiéndose en elementos interactivos en el sistema. Esta capa es especialmente útil durante la fase de <strong>replanteo</strong>, ya que facilita la adición de información relevante mediante el <strong>Gestor de Documentación</strong>, como actas de replanteos y permisos. También permite llevar un conteo preciso de Unidades de Usuario (UUIs).
        </p>
        <p>
            Además, la capa de portaleros contribuye en la preparación del informe anual de <strong>Consulta Pública sobre la Identificación de Zonas Blancas NGA y Zonas Grises NGA</strong>, en cumplimiento con las normativas establecidas por el Ministerio. Esta función asegura que el despliegue de la infraestructura cumpla con los requisitos de cobertura y accesibilidad de zonas estratégicas.
        </p>
    </div>
