#################################
Opciones Generales de aGIS
#################################


.. raw:: html

   <div style="text-align: justify;">
       <p>
           En el menú desplegable de la compañía, se encuentran disponibles las opciones <strong>Generales de Compañía</strong> y <strong>Perfil</strong>, para una configuración personalizada de la plataforma.
       </p>
   </div>

.. figure:: images/modulo/02_interfaz/image44.png
   :align: center
   :width: 30%

   Configuración de aGIS: Compañía, Perfil y Cerrar Sesión



Opciones de Compañía (solo usuarios administradores)
====================================================

.. raw:: html

   <div style="text-align: justify;">
       <p>
           De forma suplementaria, al pulsar sobre nuestro <strong>alias</strong> y la <strong>organización</strong>, se desplegará el menú de <strong>Perfil</strong>. Esta pestaña permite administrar todos los datos de nuestro usuario, así como toda la información y gestión correspondiente a la organización o compañía.
       </p>
   </div>


.. raw:: html

   <div style="text-align: justify;">
       <p>
           Como se observa en la imagen, la interfaz se encuentra dividida en dos grupos. El primer grupo, que abarca desde <strong>compañía</strong> hasta <strong>opciones</strong>, está reservado exclusivamente para los administradores de la organización. El segundo grupo, que incluye desde <strong>notificaciones</strong> hasta <strong>log out</strong>, está disponible para todos los usuarios de aGIS.
       </p>
   </div>

Compañía
--------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           <strong>Datos relativos a la organización o compañía de la cuenta.</strong> Modificable por el administrador:
       </p>
       <ul>
           <li><strong>Nombre:</strong> Nombre de la organización.</li>
           <li><strong>Teléfono:</strong> Teléfono de contacto de la organización.</li>
           <li><strong>E-Mail:</strong> Mail de Notificaciones de la organización.</li>
           <li><strong>Logo:</strong> Logo usado por la organización.</li>
       </ul>
   </div>

.. figure::  images/modulo/02_interfaz/image57.png
   :align:   center
   :width: 80%

   Configuración de Compañía

.. raw:: html

   <div style="margin-bottom: 20px;"></div>
    <br>

Equipos de Trabajo
------------------

.. raw:: html

   <div style="text-align: justify;">
       <p>
            aGIS permite la creación de Grupos de Trabajo, un nivel inferior al de la compañía, donde los administradores (TEAMLEADERS) podrán gestionar los permisos de un conjunto de usuarios y proyectos que estén asociados a dicho grupo.
            En caso de pertenecer a un Grupo de Trabajo, siendo administrador o no, éste aparecerá en el MENÚ. Además, en caso de ser administrador, se nos habilitará como botón para poder gestionar los permisos de los mismos.
        </p>
   </div>

.. figure::  images/modulo/02_interfaz/image293.png
   :align:   center
   :width: 80%

   Configuración Equipos de Trabajo

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Dentro de la <strong>configuración</strong>, se seleccionarán los <strong>perfiles</strong> (usuarios) a integrar en el equipo. Una vez seleccionados, se volverá a elegir el/los <strong>administrador/es</strong> del equipo (a partir de ahora <strong>Teamleaders</strong>). Por último, se seleccionará el/los <strong>proyecto/s</strong> a integrar en dicho equipo de trabajo.
       </p>
       <p>
           <strong>Usaremos la tecla "Ctrl" para una selección múltiple.</strong>
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/image295.png
   :align:   center
   :width: 80%

   Configuración de usuarios y proyectos en Equipos de Trabajo

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Una vez terminada la <strong>configuración</strong> del equipo de trabajo, se pulsa el botón <strong>guardar</strong> para aplicar los cambios, y de este modo tendremos el equipo totalmente configurado.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/image296.png
   :align:   center
   :width: 80%

   Resultado tras la configuración de usuarios y proytectos


.. raw:: html

   <div style="text-align: justify;">
       <p>
           Los <strong>TEAMLEADERS</strong> de los equipos de trabajo podrán configurar los <strong>permisos</strong> entre los <strong>PROYECTOS</strong> y <strong>USUARIOS</strong> de dicho equipo, atendiendo al máximo número de permisos del que el <strong>TEAMLEADER</strong> disponga.
       </p>
   </div>


.. figure::  images/modulo/02_interfaz/image297.png
   :align:   center
   :width: 80%

   Configuración por TeamLeaders


Usuarios
--------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Muestra la <strong>información de usuarios</strong> de la organización. Esta opción está disponible únicamente en caso de ser <strong>administrador de compañía</strong>.
       </p>
       <p>
           <strong>Acciones disponibles:</strong>
       </p>
       <ul>
           <li>
               <strong>Crear Usuarios:</strong>
               Esta opción está disponible siempre que la tarifa contratada lo permita. Si se ha alcanzado el límite, puede hacer clic en <strong>Contacto</strong> para hablar directamente con el departamento comercial de Stratos Global Solutions a través de <a href="https://wa.me/614335068" target="_blank"><strong>WhatsApp</strong></a>.
           </li>
           <li>
               <strong>Cambiar Contraseña:</strong>
               Si hacemos clic sobre la <strong>llave</strong>, podemos cambiar la contraseña de un usuario, ya sea estableciendo una nueva o generándola aleatoriamente.
           </li>
           <li>
               <strong>Borrado:</strong>
               Está disponible la acción de <strong>borrado</strong> para aquellos usuarios que no son administradores, permitiendo así eliminar el usuario de la plataforma.
           </li>
       </ul>
   </div>




.. figure::  images/modulo/02_interfaz/image100.png
   :align:   center
   :width: 80%

   Configuración de Usuarios

Proyectos
---------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Los administradores de la compañía tienen la capacidad de añadir nuevos proyectos. La cantidad y características de estos proyectos estarán sujetas a las limitaciones establecidas en la tarifa contratada.
       </p>
   </div>


.. figure::  images/modulo/02_interfaz/perfil_proyecto_1.png
   :align:   center
   :width: 80%

   Configuración de Proyectos


.. raw:: html

   <div style="text-align: justify;">
       <p>
           Para añadir un nuevo proyecto, es necesario hacer clic en la opción <strong>Añadir</strong>.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/perfil_proyecto_2.png
   :align:   center
   :width: 80%

   Creación de Proyectos

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Procedemos a completar el nombre del proyecto y a establecer las coordenadas donde se llevará a cabo. Para ello, podemos hacer clic directamente en el mapa o ingresar manualmente las distintas coordenadas.
       </p>
       <p>
           De esta forma, se configurarán las coordenadas y el nivel de zoom inicial que se mostrarán al iniciar el proyecto o al hacer clic en el botón <strong>Home</strong> del mapa (esto se explicará más adelante).
       </p>
       <p>
           Asimismo, deberemos seleccionar el servidor de mapas más reciente (S15) y, por último, proceder a guardar.
       </p>
       <p>
           Por último, es importante señalar que esta operación puede tardar varios minutos antes de que el proyecto aparezca en el selector de proyectos en el encabezado de la plataforma.
       </p>
   </div>
   <br>

Permisos
--------

.. raw:: html

   <div style="text-align: justify;">
       <p>
           Otorga a los administradores de la compañía el control sobre la asignación de proyectos. Dentro de cada proyecto, se podrán definir permisos específicos para cada usuario.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/image187.png
   :align:   center
   :width: 80%

   Configuración de Permisos


.. figure::  images/modulo/02_interfaz/image187b.png
   :align:   center
   :width: 80%

   Configuración de Permisos


Consulta de permisos
++++++++++++++++++++

.. raw:: html

   <div style="text-align: justify;">
       <p>
           A través de la ventana superior, que se puede visualizar en la captura anterior, es posible verificar lo siguiente:
       </p>
       <ul>
           <li>Seleccionando un usuario</li>
       </ul>
       <p>
           Si seleccionamos un usuario, el sistema mostrará en la parte derecha los proyectos que le han sido asignados.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/image140.png
   :align:   center
   :width: 80%

   Consulta de proyectos asignados a un usuario.

* Seleccionando un proyecto

Si solo seleccionamos un proyecto, nos mostrará los usuarios que tiene asignados en la parte derecha.


.. figure::  images/modulo/02_interfaz/image151.png
   :align:   center
   :width: 80%

   Consulta de los usuarios asignados a un proyecto.



.. |image56| image:: images/modulo/02_interfaz/image56.png
       :width: 80

.. raw:: html

   <div style="text-align: justify;">
        <p>Si seleccionamos ambos, y pulsamos en el botón
            <a class="reference internal" href="../../_images/image56.png"><img alt="image56" src="../../_images/image56.png" style="width: 150px;"></a>,
            nos mostrará los permisos que tiene asignados el usuario dentro del proyecto.
        </p>
   </div>


.. figure::  images/modulo/02_interfaz/image82.png
   :align:   center
   :width: 80%

   Permisos de un usuario en un proyecto


.. |image13| image:: images/modulo/02_interfaz/image13.png
       :width: 80

.. |image64| image:: images/modulo/02_interfaz/image64.png
       :width: 80

Edición de permisos
+++++++++++++++++++

.. raw:: html

   <div style="text-align: justify;">
        <p>Para editar permisos, primero debemos vincular un usuario a un proyecto y luego asignarle los permisos.</p>

        <p>Para asociar usuarios a un proyecto, es necesario seleccionar a los usuarios de la organización que se desee, mediante doble clic o haciendo clic en
            <a class="reference internal" href="../../_images/image13.png"><img alt="Seleccionar usuario" src="../../_images/image13.png" style="width: 25px;"></a>.
            Si se desea seleccionar a todos los usuarios, se debe pulsar en
            <a class="reference internal" href="../../_images/image64.png"><img alt="Seleccionar todos" src="../../_images/image64.png" style="width: 25px;"></a>.
        </p>
    </div>


.. figure::  images/modulo/02_interfaz/image148.png
   :align:   center
   :width: 80%

   Selección de usuarios no administradores


.. |image94| image:: images/modulo/02_interfaz/image94.png
       :width: 80

.. |image87| image:: images/modulo/02_interfaz/image87.png
       :width: 80

.. raw:: html

   <div style="text-align: justify;">
        <p>Una vez elegidos los usuarios, seleccionamos el proyecto al que los deseemos vincular y pulsamos en
            <a class="reference internal" href="../../_images/image94.png"><img alt="Vincular" src="../../_images/image94.png" style="width: 100px;"></a>
            para vincular y en
            <a class="reference internal" href="../../_images/image87.png"><img alt="Desvincular" src="../../_images/image87.png" style="width: 100px;"></a>
            para desvincular.
        </p>
    </div>


.. figure::  images/modulo/02_interfaz/image128.png
   :align:   center
   :width: 80%

   Resultado de la asociación/desasociación de usuarios


.. |image170| image:: images/modulo/02_interfaz/image170.png
       :width: 80

.. raw:: html

   <div style="text-align: justify;">
        <p> Para asignar los permisos, seleccionamos los usuarios del proyecto y seleccionamos los permisos que deseemos haciendo doble clic o pulsando en
            <a class="reference internal" href="../../_images/image13.png"><img alt="Asignar" src="../../_images/image13.png" style="width: 25px;"></a>.
            Para seleccionarlos todos, debemos pulsar el botón
            <a class="reference internal" href="../../_images/image64.png"><img alt="Desasignar" src="../../_images/image64.png" style="width: 25px;"></a>
            .
        </p>

        <p> Para finalizar le damos a aplicar
            <a class="reference internal" href="../../_images/image170.png"><img alt="Aplicar" src="../../_images/image170.png" style="width: 100px;"></a>.
        </p>
    </div>
    <br>


Tipos de permisos
+++++++++++++++++

Dependiendo de las necesidades y las limitaciones que deseemos aplicar disponemos de los siguientes permisos:

* **TELCO - VISUALIZACIÓN:**
    Permite la visualización de los elementos sobre el mapa.

* **TELCO - EDICIÓN:**
    Permite la edición de los elementos que se muestran en el mapa.

* **TELCO - SERVICIOS LÓGICOS:**
    Permite acceder a la herramienta lógica de red.

* **TELCO - INVENTARIO:**
    Permite acceder a la herramienta inventario de red.

* **INDOOR - VISUALIZACIÓN:**
    Permite la visualización de los elementos que se encuentran en instalaciones de interior.

* **INDOOR - EDICIÓN:**
    Permite acceder a los elementos de gestión indoor.

* **GESTOR COBERTURA:**
    Permiso que nos permite administrar las áreas de cobertura además de acceder a sus funcionalidades en el buscador.

* **GESTOR SUCS:**
    Permite acceder a los elementos de SUC además de poder gestionarlos y realizar un inventario de los mismos.

* **GESTOR MEDIDAS REFLECTOMÉTRICAS:**
    Permite acceder a la herramienta de visualización, creación y gestión de las medidas reflectométricas.

* **GESTOR DOCUMENTAL:**
    Permite acceder a la herramienta gestor documental.

* **GESTOR TRABAJOS:**
    Permite acceder a la herramienta gestor de trabajos.

* **GESTOR INCIDENCIAS:**
    Permite acceder a la herramienta gestor de incidencias.


Accesos
-------

.. raw:: html

   <div style="text-align: justify;">
        <p> Nos permite ver el registro de actividad de los usuarios de nuestra compañía, mostrando la fecha y hora,
            el nombre de usuario, el proyecto y la actividad realizada. También nos permite exportarlo a diferentes
            formatos para conservar los registros.
        </p>
   </div>


.. figure::  images/modulo/02_interfaz/image22.png
   :align:   center
   :width: 80%

   Registro de actividad de usuarios


Mapas Base
----------

.. raw:: html

   <div style="text-align: justify;">
        <p> En la sección de <strong>mapas base</strong>, es posible agregar servicios <strong>TMS/WMS/WMTS</strong> con el objetivo de proporcionar información adicional al proyecto.</p>
        <br>
        <p>En primer lugar, se muestra un listado con los <strong>mapas base actuales</strong>.</p>
   </div>

.. figure::  images/modulo/02_interfaz/listar_maps.png
   :align:   center
   :width: 80%

   Listado de mapas base añadidos.

.. raw:: html

   <div style="text-align: justify;">
        <p> Para agregar un nuevo servicio, pulsamos el botón <strong>"Añadir"</strong>, lo que desplegará un <strong>formulario</strong> en el cual se deberán completar los <strong>campos requeridos</strong>.</p>
        <br>
        <p><strong>Ejemplo:</strong></p>
   </div>

.. figure::  images/modulo/02_interfaz/add_maps.png
   :align:   center
   :width: 100%

   Añadir nuevo mapa base.

.. raw:: html

   <div style="text-align: justify;">
        <p>
            Una vez guardado, seremos redirigidos al <strong>listado de servicios disponibles</strong>.

            En la columna <strong>"Acción"</strong>, se podrá <strong>editar</strong> o <strong>eliminar</strong> cualquier servicio.

            * Estas acciones solo estarán disponibles para los usuarios que pertenezcan a la <strong>misma organización</strong>.
        </p>
   </div>
    <br>




Potencias
----------

.. raw:: html

   <div style="text-align: justify;">
        <p>
            En la sección de Potencias, es posible establecer los valores máximos y mínimos, así como los umbrales para las
            Potencias <strong>Transmitidas</strong> y <strong>Recibidas</strong> dentro de nuestra organización.
        </p>
   </div>

.. figure::  images/modulo/02_interfaz/image290.png
   :align:   center
   :width: 90%

   Configuración de Potencias de la organización.

.. raw:: html

   <div style="text-align: justify;">
        <p>
            Este apartado solo podrá ser modificado y visualizado por usuarios con perfil de administrador en la organización y
            con los permisos correspondientes del Módulo de Medidas Reflectométricas.
        </p>
   </div>
   <br>



Opciones
--------

.. raw:: html

   <div style="text-align: justify;">
        <p>
            En la sección de <strong>Opciones</strong> se encuentran las principales categorías de configuración disponibles dentro de la interfaz.
        </p>
        <p>
            Estas opciones se organizan en dos partes bien diferenciadas, cada una de ellas estructurada en función de la sección a la que afectan.
        </p>
   </div>
   <br>


.. figure::  images/modulo/02_interfaz/image155.png
   :align:   center
   :width: 100%

.. figure::  images/modulo/02_interfaz/image155b.png
   :align:   center
   :width: 100%

   Opciones de Organización

.. raw:: html

   <div style="text-align: justify;">
   </div>
   <br>


.. figure::  images/modulo/02_interfaz/image155_gestionproyectos.png
   :align:   center
   :width: 100%

   Opciones de Gestión de Proyectos


.. raw:: html

   <div style="text-align: justify;">
        <p>
            En la sección de <strong>Gestión de Proyectos</strong> se encuentran las opciones para configurar el selector de proyectos.
            Los proyectos MDB corresponden a proyectos que contienen una <strong>M</strong>ulti<strong>D</strong>ata<strong>B</strong>ase,
            diseñados para manejar proyectos de gran envergadura de ámbito nacional o continental que integran otros más pequeños,
            permitiendo una visualización adecuada. Un ejemplo de esto es la posibilidad de visualizar un mapa general de un pais que
            incluye los <strong>Territorios MDB</strong>.Al hacer zoom en uno de estos territorios, la plataforma se conectará
            automáticamente a la base de datos correspondiente.
        </p>
   </div>
   <br>

.. raw:: html

   <div style="text-align: justify;">
        <p>
            En la siguiente sección, configuraremos cómo deseamos que las ventanas de información presenten los datos, de acuerdo con las indicaciones descritas a continuación:
        </p>
   </div>

.. figure::  images/modulo/02_interfaz/image155_infowindows.png
   :align:   center
   :width: 100%

   Opciones de InfoWindows

.. raw:: html

   <div style="text-align: justify;">
        <br>
        <ul>
            <li>Si activamos la opción <strong>Ventana de información de cables avanzada</strong>, al acceder a los
                esquemas, observaremos cómo los servicios se sombrean en función de su ocupación.
            </li>
        </ul>

   </div>
   <br>

.. figure::  images/modulo/02_interfaz/image67.png
   :align:   center
   :width: 50%

   Activación de la casilla

.. raw:: html

   <div style="text-align: justify;">
        <br>
        <p>
            En caso de no seleccionar esta opción, la visualización será la siguiente:
        </p>
   </div>
   <br>


.. figure::  images/modulo/02_interfaz/image120.png
   :align:   center
   :width: 50%

   Sin marcar la casilla



.. raw:: html

   <div style="text-align: justify;">
        <ul>
            <li>Si activamos la vista avanzada, en el gestor de conexiones vemos los servicios activos inyectados.</li>
        </ul>
   </div>

.. figure::  images/modulo/02_interfaz/image54.png
   :align:   center
   :width: 50%

   Marcando la casilla


.. figure::  images/modulo/02_interfaz/image62.png
   :align:   center
   :width: 50%

   Sin marcar la casilla


.. raw:: html

   <div style="text-align: justify;">
        <ul>
            <li>Si activamos la vista de segmentos clásica.</li> <!-- TODO AGG: Realizar.-->
        </ul>
   </div>


.. raw:: html

   <div style="text-align: justify;">
        <ul>
            <li>Si activamos la opción de <strong>Gestión por Plantas</strong>, se habilitarán unos botones para subir o bajar entre plantas,
                permitiendo diseñar y separar el proyecto por niveles.</li>
        </ul>
   </div>


.. |image299| image:: images/modulo/02_interfaz/image299.png
       :width: 80

.. |image299b| image:: images/modulo/02_interfaz/image299b.png
       :width: 80



.. raw:: html

   <table style="width: 100%; border-collapse: collapse; border: none;">
       <tr>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image299.png">
                   <img alt="Marcando la casilla" src="../../_images/image299.png" style="height: 300px;">
               </a>
               <p>Marcando la casilla</p>
           </td>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image299b.png">
                   <img alt="Sin marcar la casilla" src="../../_images/image299b.png" style="height: 300px;">
               </a>
               <p>Sin marcar la casilla</p>
           </td>
       </tr>
   </table>
   <br>


.. raw:: html

   <div style="text-align: justify;">

        <p> En la sección <strong>Edición</strong> se presentan dos opciones principales. La primera de ellas es la función de Snapping,
            que permite que los elementos se alineen automáticamente al digitalizarlos. Esta función distingue entre los elementos que
            deben unirse, evitando la alineación de aquellos que no tienen relación entre sí.
            <br>
            Además de habilitar esta función, es posible seleccionar las capas con las que se desea trabajar mediante un menú desplegable.
            Esto facilita la edición al evitar la necesidad de activar o desactivar capas manualmente según las necesidades del diseño.
        </p>

        <p>
            Por otro lado, el gestor de duplicados permite agregar una partícula al nombre de cada elemento (según la opción seleccionada
            en el selector) o no añadirla, lo que facilita el control de duplicados en la red. Se recomienda, por defecto, agregar partículas
            para evitar la existencia de elementos duplicados.
        </p>
   </div>


.. figure::  images/modulo/02_interfaz/image300.png
   :align:   center
   :width: 100%

   Configuración de Edición


.. raw:: html

   <div style="text-align: justify;">

        <p> Continuando con la página de <strong>Opciones</strong> nos encontramos con tres bloques.</p>
   </div>

.. figure::  images/modulo/02_interfaz/image301.png
   :align:   center
   :width: 100%

   Configuración Equipos, SUC y Catastros Integrados.


.. raw:: html

   <div style="text-align: justify;">
        <p> En la sección <strong>Equipos</strong> permitiremos que en el diseño en nuestro <strong>Mapa</strong> se realice
            con elementos ya predefinidos por el administrador del proyecto. Es decir, en caso de activar la opción, sólo
            se podrán utilizar los fabricantes/modelos cargados por la organización. En caso contrario, se puede digitalizar
            cualquier elemento en la red sea o no ya predefinido por el administrador de la organización.
        </p>
   </div>


.. |image302| image:: images/modulo/02_interfaz/image302.png
       :width: 80

.. |image302b| image:: images/modulo/02_interfaz/image302b.png
       :width: 80

.. raw:: html

   <table style="width: 100%; border-collapse: collapse; border: none;">
       <tr>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image302.png">
                   <img alt="Marcando la casilla" src="../../_images/image302.png" style="height: 300px;">
               </a>
               <p>Marcando la casilla</p>
           </td>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image302b.png">
                   <img alt="Sin marcar la casilla" src="../../_images/image302b.png" style="height: 300px;">
               </a>
               <p>Sin marcar la casilla</p>
           </td>
       </tr>
   </table>
   <br>


.. raw:: html

   <div style="text-align: justify;">
        <p> En la sección <strong>Civil y Gestión de SUCs</strong> podremos digitalizar información sobre las SUCs asociadas
            en la canalización seleccionada. En caso de activar, el gestor de SUCs no estará integrado y solo
            se podrán ver desde su utilidad en el menú principal de la izquierda.
        </p>
   </div>



.. |image303| image:: images/modulo/02_interfaz/image303.png
       :width: 80

.. |image303b| image:: images/modulo/02_interfaz/image303b.png
       :width: 80

.. raw:: html

   <table style="width: 100%; border-collapse: collapse; border: none;">
       <tr>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image303.png">
                   <img alt="Marcando la casilla" src="../../_images/image303.png" style="height: 300px;">
               </a>
               <p>Marcando la casilla</p>
           </td>
           <td style="text-align: center; border: none;">
               <a class="reference internal" href="../../_images/image303b.png">
                   <img alt="Sin marcar la casilla" src="../../_images/image303b.png" style="height: 300px;">
               </a>
               <p>Sin marcar la casilla</p>
           </td>
       </tr>
   </table>
   <br>

.. raw:: html

   <div style="text-align: justify;">
        <p> En la sección de <strong>Catastros Integrados</strong>, se habilitará una nueva funcionalidad en el mapa.
            Al seleccionar un punto en el <strong>Mapa</strong>, se obtendrá información detallada sobre la parcela seleccionada.
        </p>
   </div>

.. figure::  images/modulo/02_interfaz/image304.png
   :align:   center
   :width: 80%

   Con la casilla marcada


.. figure::  images/modulo/02_interfaz/image304b.png
   :align:   center
   :width: 80%

   Sin marcar la casilla


.. |image304d| image:: images/modulo/02_interfaz/image304d.png
       :width: 80



.. raw:: html

   <div style="text-align: justify;">
        <p> Al marcar la casilla, además de obtener la información de la parcela, se activará una nueva funcionalidad:
            <a class="reference internal" href="../../_images/image304d.png"><img alt="AutoGescalizar" src="../../_images/image304d.png" style="width: 100px;"></a>.
            Esta funcionalidad, como se muestra en la siguiente imagen, permite acceder a los datos catastrales de la parcela seleccionada
            y al <strong>Gescal37</strong> correspondiente.
        </p>
   </div>

.. figure::  images/modulo/02_interfaz/image304c.png
   :align:   center
   :width: 100%

   AutoGescalizador


.. |guardar| image:: images/modulo/02_interfaz/guardar.png
       :width: 80

.. |volver| image:: images/modulo/02_interfaz/volver.png
       :width: 80


.. raw:: html

   <div style="text-align: justify;">
        <p> Por último, tras haber configurado todo lo anterior en las <strong>Opciones de mi Compañía</strong> , debemos dar a
            <a class="reference internal" href="../../_images/guardar.png"><img alt="Guardar" src="../../_images/guardar.png" style="width: 100px;"></a>.
            Para seleccionarlos todos, debemos pulsar el botón
            <a class="reference internal" href="../../_images/volver.png"><img alt="Volver" src="../../_images/volver.png" style="width: 90px;"></a>
            .
        </p>
   </div>


.. raw:: html

   <div style="text-align: justify;">
        <p> En el final de esta hoja, encontramos la posibilidad de importar <strong>Códigios de Colores</strong> por Organización.
            <a class="reference internal" href="../../_images/guardar.png"><img alt="Guardar" src="../../_images/guardar.png" style="width: 100px;"></a>.
            Para seleccionarlos todos, debemos pulsar el botón
            <a class="reference internal" href="../../_images/volver.png"><img alt="Volver" src="../../_images/volver.png" style="width: 90px;"></a>
            .
        </p>
   </div>
   <br>


Opciones de Perfil
==================

.. raw:: html

   <div style="text-align: justify;">
        <p><strong>Opciones de Perfil</strong> permite configurar los parámetros del usuario activo. Esta configuración es accesible para cualquier usuario de aGIS, ya sea administrador o no, pues se trata de una configuración individual de usuario.</p>
   </div>

.. figure:: images/modulo/02_interfaz/image305b.png
   :align: center
   :width: 100%

   Opciones de Perfil

Perfil
------

.. raw:: html

   <div style="text-align: justify;">
        <p><strong>Administrar Perfil</strong> proporciona opciones como el idioma, color del menú, fondo y la posibilidad de fijar dicho menú.</p>
   </div>

.. figure:: images/modulo/02_interfaz/image305.png
   :align: center
   :width: 100%

   Opciones de Perfil

Notificaciones
--------------

.. raw:: html

   <div style="text-align: justify;">
        <p>En la sección <strong>Notificaciones</strong> es posible gestionar las notificaciones, lo que incluye crearlas, visualizarlas en detalle o eliminarlas, dependiendo de los permisos del usuario y del tipo de notificación.</p>
   </div>

.. figure:: images/modulo/02_interfaz/notificaciones/notice.png
   :align: center
   :width: 90%

   Vista predeterminada de Notificaciones

Globales
++++++++

.. raw:: html

   <div style="text-align: justify;">
        <p>Las notificaciones globales son de carácter general para todos los usuarios de aGIS TELCO y no pueden ser administradas por ningún usuario.</p>
   </div>

.. figure:: images/modulo/02_interfaz/notificaciones/global_list_notice.png
   :align: center
   :width: 90%

   Vista de Notificaciones Globales

De la Compañía
++++++++++++++

.. raw:: html

   <div style="text-align: justify;">
        <p>Las notificaciones de la compañía están destinadas a todos los usuarios de la misma y solo pueden ser administradas por los administradores de la compañía.</p>
        <p>En la imagen, se observa la opción de <strong>Añadir</strong> y, en la columna "Acción", se muestran las opciones de edición y eliminación, disponibles únicamente para los administradores. En caso de no contar con permisos administrativos, solo aparecería la opción de <strong>Ver en detalle</strong>.</p>
        <p>Los administradores pueden añadir nuevas notificaciones de compañía completando el siguiente formulario:</p>
   </div>

.. figure:: images/modulo/02_interfaz/notificaciones/org_list_notice.png
   :align: center
   :width: 90%

   Vista de Notificaciones de la Compañía

.. figure:: images/modulo/02_interfaz/notificaciones/org_form_notice.png
   :align: center
   :width: 90%

   Formulario de Notificaciones de la Compañía

De Proyecto
+++++++++++

.. raw:: html

   <div style="text-align: justify;">
        <p>Las notificaciones de proyecto están dirigidas a los usuarios asignados a cada proyecto y pueden ser administradas tanto por los usuarios del proyecto como por los administradores de la organización.</p>
        <p>La vista muestra las notificaciones del proyecto seleccionado. En el caso de administradores, se mostrarán las notificaciones de toda la organización, sin importar el proyecto seleccionado.</p>
        <p>Todas las notificaciones son gestionables por los administradores de la compañía, y cada usuario puede administrar sus propias notificaciones, así como ver en detalle las notificaciones creadas por otros usuarios.</p>
        <p>Para añadir nuevas notificaciones de proyecto, se debe completar el siguiente formulario:</p>
   </div>

.. figure:: images/modulo/02_interfaz/notificaciones/sevc_list_notice.png
   :align: center
   :width: 90%

   Vista de Notificaciones de Proyecto

.. figure:: images/modulo/02_interfaz/notificaciones/sevc_form_notice.png
   :align: center
   :width: 90%

   Formulario de Notificaciones de Proyecto

De Usuario
++++++++++

.. raw:: html

   <div style="text-align: justify;">
        <p>Las notificaciones de usuario están dirigidas a usuarios específicos. Cada usuario puede crear sus propias notificaciones como recordatorio, y los administradores de la organización pueden asignar notificaciones a los usuarios de su organización.</p>
        <p>Cada usuario puede visualizar sus notificaciones, mientras que el administrador tiene acceso a las notificaciones de todos los usuarios.</p>
        <p>Para añadir nuevas notificaciones de usuario, se debe completar el siguiente formulario:</p>
   </div>

.. figure:: images/modulo/02_interfaz/notificaciones/prof_list_notice.png
   :align: center
   :width: 90%

   Vista de Notificaciones de Usuario

.. figure:: images/modulo/02_interfaz/notificaciones/prof_form_notice.png
   :align: center
   :width: 90%

   Formulario de Notificaciones de Usuario

Token
-----

.. raw:: html

   <div style="text-align: justify;">
        <p>Con el <strong>Token</strong> y el <strong>Usuario</strong>, es posible comunicarse directamente con la aplicación mediante la API (ver anexo). Desde esta ventana, también se puede forzar el cambio del Token de usuario. En caso contrario, el token se refresca en cada autenticación, siempre y cuando haya un intervalo de tiempo de 180 segundos entre ellas. Además, se puede consultar la creación y expiración del token.</p>
   </div>

.. figure:: images/modulo/02_interfaz/image288.png
   :align: center
   :width: 80%


Log out
==================

.. raw:: html

   <div style="text-align: justify;">
        <p>Para cerrar sesión, pulsamos en <strong>Log Out</strong>.</p>
   </div>

