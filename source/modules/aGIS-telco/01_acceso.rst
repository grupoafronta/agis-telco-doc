##############
¿Cómo Acceder?
##############


.. raw:: html

   <div style="text-align: justify;">
        aGIS es una aplicación multiplataforma completamente web, accesible desde cualquier navegador convencional. No obstante, se encuentra optimizada para los siguientes navegadores:
   </div>
    <br>

- Mozilla Firefox 66.0.5 o posterior

- Google Chrome versión 74 o posterior

Para acceder basta con ir a la URL:

.. raw:: html

   <div style="text-align: center; margin-bottom: 10px;">
       <a href="https://agis-eu.stratosgs.com">https://agis-eu.stratosgs.com</a>
   </div>


.. figure::  images/modulo/01_acceso/login.png
   :align:   center
   :width: 80%

   Ventana inicial de Login.