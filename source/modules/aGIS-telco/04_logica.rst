################
Lógica de la Red
################

.. figure::  images/modulo/04_logica/logica_red_menu.png
   :align:   center
   :width: 30%

   Menú Lógica de Red


.. raw:: html

    <div style="text-align: justify;">
        <p>
            El menú <strong>Lógica de Red</strong> se encuentra disponible únicamente cuando estamos visualizando el mapa en la aplicación. Este menú permite gestionar y crear servicios a través de diferentes herramientas diseñadas para inyectar servicios en los puertos de la red.
        </p>
        <p>
            El menú presenta cinco opciones principales:
        </p>
        <ul>
            <li><strong>Buscar Circuitos:</strong> Permite buscar servicios lógicos y circuitos ya configurados en el sistema.</li>
            <li><strong>Gestionar OLTs:</strong> Herramienta para administrar las OLTs dentro de la infraestructura de red.</li>
            <li><strong>Gestionar Circuitos Lógicos:</strong> Facilita la creación y asignación de servicios a un puerto. Este es el primer paso para configurar un servicio.</li>
            <li><strong>Gestionar Canales Ópticos:</strong> Una vez creado el circuito, esta herramienta permite seleccionar el canal óptico a través del cual se transmitirá el servicio.</li>
            <li><strong>Gestionar Capacidad:</strong> En caso de que el servicio requiera una configuración de capacidad específica, esta opción permite finalizar dicha configuración.</li>
        </ul>
        <p>
            Adicionalmente, desde el menú <strong>Opciones Admin</strong>, ubicado en el menú de la izquierda, es posible crear o modificar <strong>propietarios</strong>, <strong>usuarios</strong>, <strong>contratos</strong> y <strong>equipos terminal</strong>, proporcionando un control completo sobre los elementos que integran los servicios en la red.
        </p>
    </div>


Buscar Circuitos
****************

.. figure::  images/modulo/04_logica/busca_circuitos.png
   :align:   center
   :width: 60%

   Búsqueda de Circuitos Lógicos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Esta herramienta de <strong>Buscador de Circuitos Lógicos y PONs</strong> permite a los usuarios buscar servicios lógicos específicos en el mapa.
            A través de un formulario, se puede seleccionar el <strong>Tipo</strong> de circuito (como GPON, Fibra Oscura, o Canal de Capacidad), el <strong>Campo</strong> sobre el cual se desea buscar (como Nombre, Cliente, Proveedor o Contrato), y el <strong>Valor</strong> específico de búsqueda.
        </p>
        <p>
            Para utilizar la herramienta, el usuario debe:
            <ol>
                <li>Seleccionar el tipo de circuito desde el desplegable de "Tipo".</li>
                <li>Escoger el campo de búsqueda desde el desplegable de "Campo".</li>
                <li>Escribir el valor deseado en el campo de entrada "Valor" y hacer clic en el botón <strong>Buscar</strong>.</li>
            </ol>
            <p>Esto facilita localizar circuitos o servicios específicos en el mapa, brindando una herramienta rápida y eficiente para la gestión de los recursos de la red.</p>
        </p>
    </div>

.. figure::  images/modulo/04_logica/resultado_circuitos.png
   :align:   center
   :width: 100%

   Resultado búsqueda de Circuitos Lógicos


Gestión de OLTs
****************************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Como se muestra en la imagen, la herramienta <strong>Gestionar OLTs</strong> permite encender y apagar los puertos GPON de nuestra <strong>OLT</strong>, facilitando la inyección de servicios en la red. Esta funcionalidad es esencial para controlar la conectividad y disponibilidad de servicios en la red de acceso, donde se gestionan tanto las <strong>tarjetas</strong> como los <strong>puertos PON</strong>.
        </p>
        <p>
            Los usuarios pueden activar o desactivar los puertos seleccionando el puerto correspondiente y haciendo clic en el botón <strong>Activar</strong> o <strong>Desactivar</strong>, según sea necesario. Esta gestión permite asegurar que los servicios activos puedan ser consultados en el mapa al utilizar la función <strong>Analizar Lógica</strong> en las ventanas de información de los segmentos.
        </p>
    </div>

.. figure:: images/modulo/04_logica/gestionar_OLTs.png
   :align: center
   :width: 100%
   :alt: Interfaz de Gestión de OLTs

   Interfaz de Gestión de OLTs


Gestión de Circuitos Lógicos
****************************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta de <strong>Gestión de Circuitos Lógicos</strong> permite añadir, editar o eliminar circuitos dentro de la red.
            Esta funcionalidad es esencial para la administración de servicios como <strong>fibra oscura</strong>, <strong>capacidad</strong> y <strong>GPON</strong>.
            En la interfaz principal, encontramos el botón para añadir circuitos, una tabla que representa los distintos circuitos y nos permite, mediante botones en la columna de acción,
            ver la localización del servicio en el mapa, editar o eliminar el circuito. La tabla se puede exportar en diferentes formatos.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image74.png
   :align: center
   :width: 80%
   :alt: Interfaz de Gestión de Circuitos Lógicos

   Interfaz de Gestión de Circuitos Lógicos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para añadir un nuevo circuito, se despliega un formulario con los siguientes campos: <strong>nombre</strong>, <strong>usuario</strong>, <strong>equipo terminal</strong> y <strong>tipo de servicio</strong>.
            Los tipos de servicio disponibles incluyen <strong>fibra oscura</strong>, <strong>capacidad</strong> y <strong>GPON</strong>.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image84.png
   :align: center
   :width: 80%
   :alt: Formulario de Creación de Circuitos Lógicos

   Formulario de Creación de Circuitos Lógicos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Una vez creado el circuito, podemos hacer clic en el icono de edición para modificar los campos del formulario inicial,
            ver o añadir <strong>puertos inyectados</strong> de los <strong>patch</strong> o de las bandejas, y gestionar los <strong>POPs intermedios</strong>.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image124.png
   :align: center
   :width: 80%
   :alt: Gestión de POPs Intermedios y Puertos Inyectados

   Gestión de POPs Intermedios y Puertos Inyectados

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para añadir un POP intermedio, simplemente hacemos clic en el botón de añadir y seleccionamos el POP correspondiente.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image1.png
   :align: center
   :width: 80%
   :alt: Selección de POP Intermedio

   Selección de POP Intermedio

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para añadir un puerto, hacemos clic en añadir y rellenamos el formulario.
            En la parte izquierda se encuentran los campos <strong>nombre</strong>, <strong>conector</strong> (opciones: ST, LC, SC, APC), <strong>destino: equipo</strong> y <strong>destino: puerto</strong>.
            A la derecha se muestra la información de los puertos asignables.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image146.png
   :align: center
   :width: 80%
   :alt: Formulario de Puertos Inyectados

   Formulario de Puertos Inyectados

.. figure:: images/modulo/04_logica/image97.png
   :align: center
   :width: 50%
   :alt: Información de Puertos Asignables

   Información de Puertos Asignables

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En la parte inferior, esta sección permite <strong>desasignar</strong> puertos inyectados, seleccionar un tipo de equipo
            (repartidores, cajas de empalme o torpedos), elegir un equipo, seleccionar un subequipo y, finalmente, un puerto.
            Para inyectar un servicio en un puerto, es necesario completar los pasos del 1 al 3.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image153.png
   :align: center
   :width: 80%
   :alt: Inyección de Servicios en Puertos

   Inyección de Servicios en Puertos


Canales Ópticos
***************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Canales Ópticos</h4>
        <p>
            La herramienta de <strong>Canales Ópticos</strong> permite asignar circuitos a los canales ópticos de las fibras.
            La interfaz muestra el botón para añadir canales ópticos y una tabla de canales, con opciones de edición y eliminación.
            La tabla se puede exportar en varios formatos.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image144.png
   :align: center
   :width: 80%
   :alt: Interfaz de Gestión de Canales Ópticos

   Interfaz de Gestión de Canales Ópticos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al añadir un canal óptico, debemos seleccionar el <strong>circuito</strong>, asignarle un <strong>nombre</strong>, elegir la <strong>capacidad</strong> y seleccionar el <strong>canal</strong>.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image81.png
   :align: center
   :width: 80%
   :alt: Formulario de Creación de Canales Ópticos

   Formulario de Creación de Canales Ópticos

Gestión de Capacidad
********************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestión de Capacidad</h4>
        <p>
            La herramienta de <strong>Gestión de Capacidad</strong> permite asignar servicios de capacidad a los canales ópticos de las fibras.
            Al igual que en las secciones anteriores, tenemos el botón para añadir el servicio de capacidad, una tabla de servicios con opciones de edición y eliminación,
            y opciones de exportación de la tabla.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image147.png
   :align: center
   :width: 80%
   :alt: Interfaz de Gestión de Capacidad

   Interfaz de Gestión de Capacidad

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En el formulario de servicio de capacidad, seleccionamos el <strong>canal óptico</strong>, asignamos un <strong>ID de servicio</strong>, definimos la <strong>capacidad</strong> y,
            opcionalmente, especificamos el <strong>origen</strong> y <strong>destino</strong> del servicio.
        </p>
    </div>

.. figure:: images/modulo/04_logica/image78.png
   :align: center
   :width: 80%
   :alt: Formulario de Gestión de Capacidad

   Formulario de Gestión de Capacidad