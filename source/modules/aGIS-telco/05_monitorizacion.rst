###############
Monitorización
###############

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Desde este menú podremos acceder a los <strong>servidores de monitorización</strong> de nuestro proyecto.
        </p>
    </div>

Servidores
-----------

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Este es el panel de administración de los <strong>servidores (TELCO-MONITOR)</strong> que son consumidos para la monitorización de los distintos puntos del mapa. Desde dicho panel, podemos añadir nuevos servidores, borrarlos o editarlos.
        </p>
    </div>

.. figure:: images/modulo/07_monitorizacion/server_monitor_list.png
   :align: center
   :width: 80%
   :alt: Panel de administración de servidores de monitorización

   Panel de administración de servidores de monitorización

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para añadir un nuevo servidor, simplemente seleccionamos <strong>Añadir</strong> y rellenamos los campos requeridos con la información del servidor que deseemos añadir.
        </p>
    </div>

.. figure:: images/modulo/07_monitorizacion/server_monitor_form.png
   :align: center
   :width: 80%
   :alt: Formulario para añadir un nuevo servidor de monitorización

   Formulario para añadir un nuevo servidor de monitorización



Monitorización
-----------------

.. |icono_actualizar| image:: images/modulo/03_funcionalidades/icono_actualizar.png
       :width: 30


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Las <strong>ventanas de información de la monitorización</strong> nos ofrecen la opción de actualizar los datos de monitoreo mediante el botón
            <img src="images/modulo/03_funcionalidades/icono_actualizar.png" alt="Icono Actualizar" style="width: 30px;">.
            Esta actualización dependerá del servidor de monitorización y el número de serie del dispositivo.
        </p>
        <p>
            Los datos monitorizados incluyen los siguientes parámetros:
        </p>
        <ul>
            <li><strong>Servidor:</strong> Servidor Telco Monitor.</li>
            <li><strong>Dirección:</strong> Dirección postal en la que se encuentra el servidor.</li>
            <li><strong>Número de serie:</strong> Número de serie de la ONT.</li>
            <li><strong>Power:</strong> Estado encendido/apagado.</li>
            <li><strong>PTX:</strong> Potencia transmitida.</li>
            <li><strong>PRX:</strong> Potencia recibida.</li>
            <li><strong>Temperatura:</strong> Temperatura de la ONT.</li>
            <li><strong>Fecha de medida:</strong> Fecha en la que se actualizó por última vez.</li>
            <li><strong>Config:</strong> OLT / SLOT / PORT / ONT.</li>
        </ul>
    </div>



.. figure::  images/modulo/03_funcionalidades/infowindow_monitor.png
   :align:   center
   :width: 50%