##########
Utilidades
##########

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En este menú, encontramos una serie de herramientas que complementan las utilidades de <strong>aGIS</strong>
            ya descritas en capítulos anteriores.
        </p>
    </div>

.. figure::  images/modulo/05_utilidades/utilidades.png
   :align:   center
   :width: 30%

   Utilidades

.. raw:: html

    <div style="text-align: justify;">
        <br>
    </div>


Inventario
**********

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La herramienta de <strong>Inventario</strong> en <strong>aGIS</strong> ofrece una vista detallada de los
            <strong>equipos de red</strong> en función de la infraestructura seleccionada. Al hacer clic en el menú
            <strong>“Inventario”</strong> o <strong>“Inventario RS”</strong>, se despliega una interfaz que permite
            acceder a datos de los equipos de red.
        </p>
        <p>
            La opción de <strong>Inventario</strong> está destinada a la <strong>planta externa de la red</strong>,
            permitiendo visualizar y gestionar todos los elementos digitales de la red externa. En cambio, la opción de
            <strong>Inventario RS</strong> se orienta específicamente a la <strong>digitalización de las centrales</strong>,
            facilitando la gestión detallada de los equipos que conforman la infraestructura interna.
        </p>
        <p>
            Los datos que proporciona el inventario están organizados por <strong>capas</strong> e incluyen información
            independiente y específica de cada una de ellas. Los usuarios pueden seleccionar los datos a consultar y
            exportar según diferentes criterios: ya sea por área delimitada mediante polígono, por nivel o en general.
            Esta versatilidad permite personalizar la información de acuerdo a las necesidades del proyecto.
        </p>
        <p>
            Además, aGIS facilita la <strong>exportación de datos</strong> de inventario en varios formatos.
            Los usuarios pueden optar por exportar la información en formato <strong>CSV</strong>, <strong>Excel</strong>
            o copiarla directamente al portapapeles para su uso inmediato en otras aplicaciones. Esta funcionalidad mejora
            la accesibilidad y el manejo de los datos de inventario, optimizando el flujo de trabajo y la toma de decisiones.
        </p>
    </div>

.. figure::  images/modulo/05_utilidades/image2.png
   :align:   center
   :width: 100%

   Utilidades - Inventario


Gestor de Exportación
*********************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El <strong>Gestor de Exportación</strong> en <strong>aGIS</strong> es una herramienta potente y versátil
            que permite exportar cualquier capa dentro de nuestro proyecto, facilitando la extracción de datos de
            manera personalizada. Para acceder a esta función, vamos a <strong>Utilidades -> Exportación</strong>.
        </p>
        <p>
            La interfaz de exportación es sencilla e intuitiva. En la parte superior, se muestra un <strong>mapa interactivo</strong>
            que permite seleccionar la zona de interés. En la parte inferior, se encuentra el área donde se configuran
            los parámetros de exportación, incluyendo la selección de la capa y el formato de archivo deseado.
        </p>
        <p>
            Para iniciar la exportación, es necesario seleccionar una <strong>capa</strong> específica y el
            <strong>formato</strong> en el que se desea exportar. Luego, se puede elegir la opción <strong>NIVEL</strong>
            para exportar solo el nivel seleccionado, o <strong>GENERAL</strong> para incluir todos los niveles.
            Si no se selecciona ninguna capa en particular, el sistema procederá a descargar todas las capas disponibles en el proyecto.
        </p>
    </div>

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Además de las opciones de nivel y general, el gestor de exportación permite seleccionar una
            <strong>zona específica del mapa</strong> mediante una herramienta de polígonos. Esto resulta útil para
            exportar datos de una región particular, sin necesidad de descargar toda la información del proyecto.
        </p>
    </div>

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Otra característica destacada es la posibilidad de aplicar <strong>condiciones y filtros específicos</strong>
            para seleccionar datos concretos dentro de una capa. Esto permite exportar solo los datos relevantes,
            ajustados a criterios específicos, en los distintos formatos soportados por la herramienta (como CSV, Excel, etc.).
            La opción de exportación por niveles o de forma general proporciona aún más flexibilidad, similar a la funcionalidad de inventario.
        </p>
        <p>
            En la imagen mostrada a continuación se exportará la capa gen_segmentos completa, y de la capa gen_equipos aquellos
            los cuales cumplan que estado están Construidos o Serviciables Y además, su situación es Fachada.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image132.png
   :align: center
   :width: 100%
   :alt: Selección de Zona en el Mapa

   Ejemplo de Exportación


Centrales TESA
***************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El módulo de <strong>Centrales TESA</strong> proporciona una interfaz que permite visualizar toda la información relevante de las centrales ubicadas en España. A través de este módulo, los usuarios pueden explorar un mapa que muestra todas las centrales y hacer clic en cada una de ellas para obtener información detallada sobre cada central, incluyendo:
        </p>
        <ul>
            <li><strong>Nombre de la Central</strong></li>
            <li><strong>Código MIGA</strong></li>
            <li><strong>Código de Zona</strong></li>
            <li><strong>Cobertura</strong></li>
        </ul>
        <p>
            Además, el módulo permite realizar búsquedas específicas de una central mediante un campo de búsqueda, como se muestra en las imágenes adjuntas. Esta funcionalidad es particularmente útil para ubicar rápidamente una central específica o para tener una vista general de todas las centrales y su distribución geográfica en el territorio español.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/centrales_tesa.png
   :align: center
   :width: 100%
   :alt: Vista general del mapa de Centrales TESA

   Vista general del mapa de Centrales TESA que muestra la ubicación de todas las centrales en España.

.. figure:: images/modulo/05_utilidades/centrales_tesab.png
   :align: center
   :width: 80%
   :alt: Información detallada de una central TESA

   Información detallada de una central TESA al hacer clic en ella, mostrando datos como nombre, código MIGA, código de zona y cobertura.



Gestor de Abonados
******************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El <strong>Gestor de Abonados</strong> permite registrar de manera lógica abonados en nuestra red a través de una caja de abonados específica. Para crear un nuevo registro de abonado, simplemente haga clic en <strong>«añadir»</strong> y complete el formulario solicitado. Además, el sistema permite la exportación de datos a diferentes formatos o la opción de impresión.
        </p>
        <ul>
            <li><strong>Datos de Contacto (Nombre, apellidos, email, teléfono):</strong> Información de contacto del nuevo abonado.</li>
            <li><strong>Dirección:</strong> La dirección se puede introducir en el plano, lo que permitirá que los campos de dirección se completen automáticamente. En caso de direcciones más complejas, existen campos adicionales para completarla.</li>
            <li><strong>OLT - ONT:</strong> Ingrese los valores correspondientes al número de serie de la ONT y los datos de conexión a la OLT.</li>
            <li><strong>Terminal:</strong> Los datos de la terminal se asignan desde el mapa. Para ello, diríjase a la caja de abonados donde será asignado el nuevo abonado, seleccione una de las terminales libres de la caja y asignela. En caso de error, hay un botón para limpiar la terminal y reasignarla de manera fácil.</li>
        </ul>
    </div>

.. figure:: images/modulo/05_utilidades/term_abon.png
   :align: center
   :width: 80%
   :alt: Selección de Terminal Abonado

   Selección de Terminal Abonado en el Mapa

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Una vez se hayan completado los datos del formulario, haga clic en <strong>«guardar»</strong> para registrar al abonado en el sistema.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/form_abon.png
   :align: center
   :width: 80%
   :alt: Formulario de Registro de Abonados

   Formulario de Registro de Abonados

.. figure:: images/modulo/05_utilidades/tabla_abon.png
   :align: center
   :width: 80%
   :alt: Tabla de Abonados Registrados

   Tabla de Abonados Registrados

.. raw:: html

    <div style="text-align: justify;">
        <p>
            En la <strong>Tabla de Abonados</strong>, se encuentra una columna de <strong>Acción</strong> que ofrece la opción de actualizar los datos o eliminar el registro de un abonado específico.
        </p>
        <p>
            Los abonados registrados también se pueden visualizar en el mapa general de la red. Al seleccionar una caja de abonados, aparecerá un apartado del <strong>«Gestor de Abonados»</strong> que muestra una tabla con los abonados registrados en esa caja. Estos abonados estarán representados en el mapa con una línea que conecta la caja de abonados a su dirección registrada.
        </p>
        <p>
            En la tabla de abonados, la columna <strong>Acción</strong> permite seleccionar la línea correspondiente al abonado específico en el mapa, facilitando su visualización.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/gest_abon.png
   :align: center
   :width: 80%
   :alt: Gestor de Abonados en el Mapa

   Vista del Gestor de Abonados en el Mapa

.. figure:: images/modulo/05_utilidades/sel_abon.png
   :align: center
   :width: 80%
   :alt: Visualización de Abonado en el Mapa

   Visualización de Abonado en el Mapa


Gestor de SUCs
**************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestor de SUCs</h4>
        <p>
            La herramienta <strong>Gestor de SUCs</strong> permite visualizar datos específicos de la capa <strong>SUC</strong> (Solicitud de Uso Compartido). Los datos que se devuelven incluyen el <strong>estado de la SUC</strong>, <strong>fechas asociadas</strong> y los <strong>documentos disponibles</strong> relacionados.
            Por defecto, el sistema muestra un recuento de los diferentes datos en forma de diagrama, pero el usuario puede alternar entre visualizar el recuento o la longitud que abarcan mediante el botón que se encuentra en la parte inferior de la interfaz.
        </p>
        <p>
            Al igual que en otras herramientas de aGIS, los datos de SUCs se pueden seleccionar para exportación por polígono, por nivel o en general. Los resultados se pueden exportar en formatos <strong>CSV</strong>, <strong>Excel</strong> o copiar directamente al portapapeles para su fácil manejo.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image104.png
   :align: center
   :width: 80%
   :alt: Diagrama de Estado de SUC

   Visualización de Estado de SUC en Diagrama

.. figure:: images/modulo/05_utilidades/image158.png
   :align: center
   :width: 80%
   :alt: Visualización de Fechas y Documentos de SUC

   Visualización de Fechas y Documentos Disponibles en SUC



Gestor de Urbas
***************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Esta herramienta permite gestionar <strong>Catastros Propios URBA</strong> para municipios de España, en función de la disponibilidad del catastro del <strong>Instituto Geográfico Nacional (IGN)</strong>.
        </p>
        <p>
            La interfaz principal muestra una tabla con el listado de proyectos asignados al usuario, junto con los <strong>URBAs</strong> asignados a cada proyecto (con un máximo de tres URBAs por proyecto) y la compañía a la que pertenece el proyecto.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image222.png
   :align: center
   :width: 90%
   :alt: Tabla de Proyectos y URBAs Asignados

   Tabla de Proyectos y URBAs Asignados al Usuario


.. |image13| image:: images/modulo/05_utilidades/iconos_urba.png
       :width: 80

.. |image223| image:: images/modulo/05_utilidades/image223.png
       :width: 80

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Para asignar un URBA a un proyecto, se debe hacer clic en el botón de edición
            <a class="reference internal" href="../../_images/image223.png"><img alt="Editar" src="../../_images/image223.png" style="width: 25px;"></a>
            de la columna <strong>Acción</strong>, el cual abrirá el formulario de asignación.
        </p>
        <p>
            Este formulario permite asignar hasta un máximo de tres URBAs a un proyecto y habilitar los iconos de URBA
            en el mapa principal de la aplicación.  <a class="reference internal" href="../../_images/iconos_urba.png"><img alt="Iconos URBA" src="../../_images/iconos_urba.png" style="width: 25px;"></a>
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image224.png
   :align: center
   :width: 80%
   :alt: Formulario de Asignación de URBA

   Formulario de Asignación de URBA a Proyecto


Gestor Documental
******************

.. |image9999| image:: images/modulo/05_utilidades/image9999.png
       :width: 25

.. |image9998| image:: images/modulo/05_utilidades/image9998.png
       :width: 20

.. |image9998| image:: images/modulo/05_utilidades/image9997.png
       :width: 20

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Con la herramienta <strong>Gestor Documental</strong>, podemos subir archivos de distintos tipos a la plataforma,
            y también exportar los ya existentes en formatos como <strong>CSV</strong>, <strong>XLS</strong>, <strong>PDF</strong>
            o imprimirlos directamente. Para crear un nuevo documento, simplemente pulsamos en el botón <strong>Añadir</strong>
            y completamos un formulario que incluye los siguientes campos:
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image141.png
   :align: center
   :width: 80%
   :alt: Formulario de Creación de Documento en Gestor Documental

   Formulario de Creación de Documento y Asociación de Archivos en Gestor Documental


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Antes de guardar un documento en el <strong>Gestor Documental</strong>, es imprescindible asociar los archivos cargados
            a un elemento en el sistema GIS, independientemente de la capa a la que pertenezca. Para ello, primero seleccionamos
            la capa de la lista desplegable, y luego utilizamos el campo de búsqueda para localizar el elemento deseado mediante
            su <strong>nombre</strong> o <strong>UUID</strong>.
        </p>
        <p>
            Una vez que encontramos el elemento, lo seleccionamos en la tabla de resultados y pulsamos el botón <strong>Asignar</strong>
            para vincular el archivo al elemento en cuestión. Posteriormente, podemos hacer clic en <strong>Guardar</strong>
            para finalizar el proceso o <strong>Cancelar</strong> si deseamos anular la acción.
        </p>
    </div>


.. raw:: html

    <div style="text-align: justify;">
        <p>
            Una vez guardamos el documento, disponemos de varias opciones: <strong>ver</strong> el archivo, haciendo clic
            en en el nombre del fichero, asignarlo a un objeto mediante
            <a class="reference internal" href="../../_images/image9999.png"><img alt="Asignar a Objeto" src="../../_images/image9999.png" style="width: 25px;"></a> o eliminarlo
            <a class="reference internal" href="../../_images/image9998.png"><img alt="Borrar" src="../../_images/image9998.png" style="width: 25px;"></a>
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image42.png
   :align: center
   :width: 80%
   :alt: Opciones para Ver, Asignar o Borrar Documentos

   Opciones para Ver, Asignar o Borrar Documentos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Si deseamos asignar el documento a un objeto, aparecerá una interfaz en la que primero seleccionamos la <strong>capa</strong> y luego elegimos el o los objetos a los que queremos vincular la documentación. Para ello, movemos los objetos de la columna de la izquierda (<strong>NON-SELECTED</strong>) a la columna de la derecha (<strong>SELECTED</strong>), y finalmente pulsamos en el botón <strong>Asignar</strong>.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image110.png
   :align: center
   :width: 80%
   :alt: Interfaz para Asignar Documentación a Objetos

   Interfaz para Asignar Documentación a Objetos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Después de completar la asignación, al hacer clic en el elemento correspondiente en el mapa, podremos ver el documento en la pestaña <strong>Gestor Documental</strong>, donde se mostrará la opción para visualizarlo.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image34.png
   :align: center
   :width: 80%
   :alt: Documento Asignado en la Pestaña Gestor Documental del Mapa

   Documento Asignado en la Pestaña Gestor Documental del Mapa


Ticketing | Operaciones
********************************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestor de Trabajo</h4>
        <p>
            En el <strong>Gestor de Trabajo</strong>, es posible observar y administrar los distintos trabajos que deben realizarse sobre la red <strong>FTTH</strong>. Para crear un nuevo trabajo, se debe pulsar sobre la opción <strong>Añadir</strong> y completar el formulario solicitado:
        </p>
        <ul>
            <li><strong>Nombre:</strong> Nombre que se le desea asignar al trabajo.</li>
            <li><strong>Descripción:</strong> Observaciones o detalles adicionales sobre el trabajo.</li>
            <li><strong>Estado:</strong> Situación actual del trabajo.</li>
            <li><strong>Capa:</strong> Capa donde se encuentra el elemento sobre el cual se realizará el trabajo.</li>
            <li><strong>Elemento:</strong> Elemento específico al que se asignará el trabajo.</li>
            <li><strong>Activar notificaciones:</strong> Predeterminadamente, el creador del trabajo recibe notificaciones, pero si se marca esta opción y se agrega un correo adicional, dicho correo también recibirá notificaciones.</li>
        </ul>
        <p>
            Una vez completado, se debe hacer clic en <strong>Guardar</strong> para registrar el trabajo.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image92.png
   :align: center
   :width: 80%
   :alt: Formulario Trabajos

   Formulario Trabajos

.. figure:: images/modulo/05_utilidades/image179.png
   :align: center
   :width: 80%
   :alt: Tabla Trabajos

   Tabla Trabajos

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al guardar un trabajo, aparece una tabla donde se muestran todos los trabajos creados junto con información relevante. En la última columna de cada fila, se encuentran opciones de acción que permiten:
        </p>
        <ul>
            <li>
                <strong>Visualizar</strong>
                <a class="reference internal" href="../../_images/image9997.png"><img alt="Visualizar" src="../../_images/image9997.png" style="width: 25px;"></a>:
                Muestra tres columnas con información sobre el trabajo. La primera columna contiene datos del trabajo, la segunda permite añadir una <strong>Resolución</strong> en formato de texto, y la tercera permite <strong>Asignar Documentación</strong> sobre el trabajo.
            </li>
            <li>
                <strong>Editar</strong>
                <a class="reference internal" href="../../_images/image216.png"><img alt="Editar" src="../../_images/image216.png" style="width: 25px;"></a>:
                Permite modificar los datos del trabajo.
            </li>
            <li>
                <strong>Eliminar</strong>
                <a class="reference internal" href="../../_images/image9998.png"><img alt="Eliminar" src="../../_images/image9998.png" style="width: 25px;"></a>:
                Permite borrar el trabajo de la lista.
            </li>
        </ul>
    </div>

.. figure:: images/modulo/05_utilidades/image103.png
   :align: center
   :width: 80%
   :alt: Visualización de Trabajo

   Visualización de Trabajo



Ticketing | Incidencias
************************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestor de Incidencias</h4>
        <p>
            El <strong>Gestor de Incidencias</strong> permite administrar las incidencias relacionadas con la red, exportarlas en diferentes formatos o imprimirlas. Para crear una nueva incidencia, se debe hacer clic en la opción <strong>Añadir</strong> y completar el formulario solicitado:
        </p>
        <ul>
            <li><strong>Nombre:</strong> Nombre que se le desea asignar a la incidencia.</li>
            <li><strong>Descripción:</strong> Observaciones o detalles adicionales sobre la incidencia.</li>
            <li><strong>Estado:</strong> Situación actual de la incidencia.</li>
            <li><strong>Capa:</strong> Capa donde se encuentra el elemento sobre el cual está la incidencia.</li>
            <li><strong>Elemento:</strong> Elemento específico al que se asignará la incidencia.</li>
            <li><strong>Activar notificaciones:</strong> Predeterminadamente, el creador de la incidencia recibe notificaciones, pero si se marca esta opción y se agrega un correo adicional, dicho correo también recibirá notificaciones.</li>
        </ul>
        <p>
            Una vez completado, se debe hacer clic en <strong>Guardar</strong> para registrar la incidencia.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image18.png
   :align: center
   :width: 80%
   :alt: Formulario Incidencias

   Formulario Incidencias

.. figure:: images/modulo/05_utilidades/image117.png
   :align: center
   :width: 80%
   :alt: Tabla Incidencias

   Tabla Incidencias

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Al guardar una incidencia, aparece una tabla donde se muestran todas las incidencias creadas junto con información relevante. En la primera columna de cada fila, se encuentran opciones de acción que permiten:
        </p>
        <ul>
            <li>
                <strong>Ver</strong>
                <a class="reference internal" href="../../_images/image9997.png"><img alt="Ver" src="../../_images/image9997.png" style="width: 25px;"></a>:
                Muestra tres columnas con información sobre la incidencia. La primera columna contiene datos de la incidencia, la segunda permite añadir una <strong>Resolución</strong> en formato de texto, y la tercera permite <strong>Asignar Documentación</strong> sobre la incidencia.
            </li>
        </ul>
    </div>

.. figure:: images/modulo/05_utilidades/image28.png
   :align: center
   :width: 80%
   :alt: Visualización de Incidencia

   Visualización de Incidencia


Medidas Reflectométricas
************************

Gestor de Medidas
-----------------

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestor de Medidas</h4>
        <p>
            El <strong>Gestor de Medidas</strong> recopila toda la información básica necesaria para gestionar las mediciones en la red. A continuación, se detalla cada una de las partes más importantes que se muestran en la interfaz.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image217.png
   :align: center
   :width: 90%
   :alt: Interfaz del Gestor de Medidas

   Interfaz del Gestor de Medidas con numeración de las secciones más importantes


1. Los botones General y Nivel.
+++++++++++++++++++++++++++++++

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La primera vez que iniciemos el <strong>Gestor de Medidas</strong>, la tabla (<strong>2</strong>) no aparecerá de inmediato; será necesario pulsar sobre uno de los botones para que se muestre la información correspondiente.
        </p>
        <ul>
            <li><strong>General:</strong> Muestra <strong>TODOS</strong> los elementos disponibles en la tabla.</li>
            <li><strong>Nivel:</strong> Muestra solo los elementos o medidas que se encuentran en el <strong>nivel (4)</strong> actual del mapa.</li>
        </ul>
        <p>
            En ambos casos, y utilizando la herramienta situada en la parte izquierda (<strong>4</strong>) del mapa, es posible dibujar una zona en el mapa y hacer clic en uno de los botones para filtrar y visualizar únicamente los elementos que se encuentran dentro del área dibujada.
        </p>
    </div>


2. La tabla
+++++++++++

.. raw:: html

    <div style="text-align: justify;">
        <p>
            La tabla es el resultado de la elección de los botones de filtrado mencionados anteriormente. En ella, encontramos una serie de campos que detallan la información de las medidas seleccionadas.
        </p>
        <p>
            El primer campo, <strong>Acción</strong>, permite acceder a opciones para <strong>editar</strong> y <strong>borrar</strong> la medida. A continuación, se muestran los campos de <strong>Cartodb_id</strong>, <strong>Nombre</strong>, <strong>Potencia Transmitida</strong>, <strong>Potencia Recibida</strong>, y <strong>Lambda</strong>, cada uno proporcionando datos específicos sobre la medida registrada.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image218.png
   :align: center
   :width: 80%
   :alt: Tabla de Medidas - Campos y Acciones

   Tabla de Medidas - Campos y Acciones

.. figure:: images/modulo/05_utilidades/image219.png
   :align: center
   :width: 80%
   :alt: Detalle de Medidas en el Gestor

   Detalle de Medidas en el Gestor


3. Botón Potencia Recibida/Transmitida
++++++++++++++++++++++++++++++++++++++

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Este botón permite visualizar en el mapa la <strong>potencia indicada</strong> de una medida. Al iniciar, el botón mostrará el texto <strong>Potencia Recibida</strong> y, al hacer clic, se desplegará dicha potencia sobre el mapa. Simultáneamente, el texto del botón cambiará a <strong>Potencia Transmitida</strong>, indicando que la próxima vez que se presione mostrará esta otra potencia en el mapa. En resumen, el texto del botón actúa como indicador del tipo de potencia que se visualizará en el mapa al siguiente clic.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image220.png
   :align: center
   :width: 80%
   :alt: Visualización de Potencia Recibida

   Visualización de Potencia Recibida en el Mapa

.. figure:: images/modulo/05_utilidades/image221.png
   :align: center
   :width: 80%
   :alt: Visualización de Potencia Transmitida

   Visualización de Potencia Transmitida en el Mapa


Inventario RS
**************

.. raw:: html

    <div style="text-align: justify;">
        <p>
            El módulo <strong>Inventario RS</strong> en <strong>aGIS</strong> permite la gestión de la <strong>planta interna</strong>
            de la red, enfocándose en la digitalización y administración detallada de las <strong>centrales</strong> y los equipos
            que componen la infraestructura interna. Esta herramienta es fundamental para visualizar y organizar la información
            específica de los elementos internos del sistema, facilitando el control sobre la infraestructura de una manera
            eficiente.
        </p>
        <p>
            Los datos proporcionados por este módulo están organizados por <strong>capas</strong> y ofrecen información independiente
            y detallada de cada elemento. Los usuarios pueden filtrar y seleccionar la información según sus necesidades,
            optando por criterios como la selección de áreas mediante <strong>polígono</strong>, <strong>nivel</strong> o de manera
            <strong>general</strong>. Esta flexibilidad permite adaptar la visualización de los datos a los requerimientos específicos
            de cada proyecto.
        </p>
        <p>
            Además, el módulo facilita la <strong>exportación de datos</strong> en varios formatos, como <strong>CSV</strong>,
            <strong>Excel</strong> o incluso copiar los datos directamente al <strong>portapapeles</strong>, lo cual optimiza el acceso
            y manejo de la información en otras aplicaciones. Esta funcionalidad mejora el flujo de trabajo y respalda una toma
            de decisiones más ágil y fundamentada.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image2.png
   :align: center
   :width: 100%
   :alt: Utilidades - Inventario RS

   Utilidades - Inventario RS



Gestor de Sites
***************

.. raw:: html

    <div style="text-align: justify;">
        <h4>Gestión de Emplazamientos de Radio</h4>
        <p>
            Esta herramienta permite la <strong>gestión de los emplazamientos de radio</strong> dentro del sistema. A través de esta función,
            se pueden asignar y organizar los datos de cada emplazamiento, vinculándolos a un <strong>POP</strong> específico.
            Para comenzar, se debe completar la información requerida en el formulario con los datos deseados, lo cual permite
            un control más estructurado sobre cada punto de radio.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image76.png
   :align: center
   :width: 80%
   :alt: Formulario de Emplazamientos de Radio

   Formulario de Emplazamientos de Radio

.. raw:: html

    <div style="text-align: justify;">
        <p>
            Una vez registrado el emplazamiento, es posible <strong>añadir diferentes elementos</strong> relacionados, como llaves, operadores, servicios, entre otros,
            proporcionando un control detallado sobre cada componente y servicio vinculado al emplazamiento. Estos elementos se configuran
            desde el menú de opciones adicionales, como se observa en la siguiente captura.
        </p>
    </div>

.. figure:: images/modulo/05_utilidades/image91.png
   :align: center
   :width: 80%
   :alt: Elementos Adicionales en Emplazamientos de Radio

   Elementos Adicionales en Emplazamientos de Radio
