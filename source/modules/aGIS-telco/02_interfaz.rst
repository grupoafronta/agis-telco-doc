#################################
Interfaz General de la Aplicación
#################################


.. raw:: html

   <div style="text-align: justify;">
       Una vez iniciada la sesión, aGIS ofrece una interfaz intuitiva y minimalista, diseñada con un estilo moderno y dinámico. Está optimizada para facilitar la ejecución eficiente de diversas tareas, garantizando una experiencia de usuario fluida. Cabe destacar que pueden presentarse ligeras variaciones en la interfaz dependiendo de la versión del navegador utilizado, manteniéndose siempre dentro de los navegadores compatibles.
   </div>
    <br>

.. figure::  images/modulo/02_interfaz/image11.png
   :align:  center
   :width: 80%

   Mapa aGIS


.. raw:: html

   <div style="text-align: justify;">
       En la interfaz principal de aGIS, se distinguen claramente tres secciones principales, que proporcionan una experiencia de usuario optimizada y bien estructurada:
       <br><br>
       - <strong>Header</strong>: Situado en la parte superior, atravesando horizontalmente toda la ventana, el encabezado permite una navegación rápida y muestra información clave del proyecto, como el nombre y detalles del usuario.
       <br><br>
       - <strong>Menú Lateral</strong>: Ubicado a la izquierda, este menú de navegación organiza las diferentes funcionalidades de la plataforma, como el acceso al <strong>Dashboard</strong>, la <strong>Visualización</strong>, la <strong>Lógica de Red</strong>, entre otras. Dentro de este menú, se destaca el botón de <strong>WhatsApp</strong>, que facilita la apertura directa de una conversación con el equipo de soporte de la plataforma, ofreciendo asistencia en tiempo real de forma rápida y sencilla.
       <br><br>
       - <strong>Panel de Interacción</strong>: Ocupando el resto del espacio en la interfaz, este panel muestra el contenido principal de las funcionalidades seleccionadas. De manera predeterminada, se muestra el <strong>Dashboard</strong>, que permite al usuario monitorear los proyectos en curso, las cuotas del sistema y otras notificaciones relevantes.
   </div>
   <br>

.. figure::  images/modulo/02_interfaz/image157.png
   :align:  center
   :width: 80%

   Dashboard aGIS

Header
======

.. raw:: html

       <div style="text-align: justify;">
       En la sección del <strong>Header</strong>, a la izquierda, se encuentra el botón para seleccionar un proyecto asignado. Además, en el icono de las tres líneas (menú), se puede ocultar o mostrar el menú lateral de la izquierda, proporcionando más espacio de trabajo en la interfaz.
       <br><br>
       </div>

.. figure:: images/modulo/02_interfaz/image126.png
   :align: center
   :width: 80%

.. raw:: html

       <div style="text-align: justify;">
        <br><br>
        En la vista del mapa, se ofrece la opción de copiar coordenadas, permitiendo al usuario buscar ubicaciones en cualquier motor de búsqueda, como Google Maps. Adicionalmente, se muestra la base de datos (BBDD) a la que se está conectado, o bien el elemento seleccionado en el mapa.
        Por último, como se verá en configuraciones posteriores, también estará disponible en esta misma ubicación la herramienta de autogescal, cuya funcionalidad será detallada más adelante.
        <br><br>
       </div>

.. figure:: images/modulo/02_interfaz/image24.png
   :align: center
   :width: 80%

.. raw:: html

   <div style="margin-bottom: 20px;"></div>
    <br>

Menú
====

.. raw:: html

   <div style="text-align: justify;">
       En la sección del <strong>menú</strong>, se encuentran los distintos apartados en los que se organiza la plataforma. Estos apartados serán explicados en detalle más adelante. Cabe destacar que los apartados disponibles pueden variar según el nivel de permisos asignado al usuario y el proyecto activo en ese momento. Por ejemplo, cuando el mapa está activo, se mostrarán las herramientas de edición, mientras que en el <strong>Dashboard</strong> estas no estarán visibles.
       <br><br>
   </div>

.. figure::  images/modulo/02_interfaz/image121.png
   :align:  center
   :width: 30%

   Menú aGIS

.. raw:: html

   <div style="margin-bottom: 20px;"></div>
    <br>

Dashboard
=========
.. raw:: html

   <div>
       <p>
           El <strong>Dashboard</strong> es la interfaz centralizada que actúa como la pantalla principal al iniciar sesión en el sistema. Esta funcionalidad permite a los usuarios acceder rápidamente a toda la información relevante relacionada con sus proyectos, proporcionando una experiencia de uso eficiente y organizada.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/image127.png
  :align:   center
  :width: 80%

  Dashboard aGIS

.. raw:: html

   <div style="text-align: justify;">
        <br><br>
       <p>
           Como se puede observar en la captura de pantalla anterior, el diseño del panel es <strong>conciso y claro</strong>, lo que facilita la navegación y la comprensión de la información presentada. La disposición de los elementos está estructurada en tres columnas, cada una con un propósito específico que contribuye a la usabilidad general del Dashboard.
       </p>

       <h4>1. Resumen de Capacidad de Diseño</h4>

       <p>
           La primera columna ofrece un <strong>resumen de la capacidad de diseño</strong> asignada al proyecto actual. Este apartado es esencial, ya que informa al usuario sobre las <strong>capas máximas</strong> que puede utilizar dentro del proyecto, desglosando la información en dos partes:
       </p>
       <ul>
           <li><strong>Capas en uso actualmente:</strong> Muestra cuántas capas están siendo utilizadas en el momento.</li>
           <li><strong>Número máximo permitido:</strong> Indica el límite superior de capas que se pueden ocupar, proporcionando un contexto claro sobre las capacidades de diseño disponibles.</li>
       </ul>
       <p>
           Este resumen permite a los usuarios gestionar de manera efectiva sus recursos y planificar su diseño sin exceder los límites establecidos en la cuota contratada.
       </p>

       <h4>2. Vista Rápida de Notificaciones</h4>
       <p>
           En la columna central se encuentra una <strong>vista rápida de notificaciones</strong>, que se divide en cuatro categorías: <strong>Globales</strong>, <strong>de Compañía</strong>, <strong>de Proyecto</strong> y <strong>de Usuario</strong>. Esta funcionalidad permite a los usuarios estar al tanto de la información crucial y las actualizaciones relacionadas con su trabajo. Al hacer clic en cualquiera de estos tipos de notificaciones, los usuarios pueden acceder a una lista detallada de las notificaciones correspondientes, lo que les permite:
       </p>
       <ul>
           <li>Revisar actualizaciones importantes.</li>
           <li>Acceder a detalles específicos de cada notificación, facilitando así un seguimiento adecuado de los eventos relevantes.</li>
       </ul>

       <h4>3. Información de la Compañía y del Usuario</h4>
       <p>
           En la tercera columna se presenta información relevante sobre la <strong>compañía</strong> y el <strong>usuario logueado</strong>. Esta sección incluye detalles sobre el <strong>tipo de plan contratado</strong>, mostrando tanto la capacidad utilizada como los límites según el tipo de plan. Además, al desplazarse hacia abajo, se puede encontrar información adicional sobre la <strong>compañía Stratos</strong>, que es la desarrolladora del sistema aGIS.
       </p>
   </div>

.. figure::  images/modulo/02_interfaz/suscripcion.png
   :align:   center
   :width: 80%

   Suscripción
