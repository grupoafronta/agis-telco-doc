############
TEST
############

I’m JUANMA, a Django developer.


H1 -- Top of Page Header
************************
There should only be one of these per page and this will also -- when
converting to pdf -- be used for the chapters.

H2 -- Page Sections
===================

H3 -- Subsection
----------------

H4 -- Subsubsection
+++++++++++++++++++

Imagen grande
+++++++++++++

.. figure::  images/modulo/login.png
   :align:   center
   :width: 80%

   Pie de foto

Imagen mediana
++++++++++++++

.. figure::  images/modulo/login.png
   :align:   center
   :width: 50%

   Pie de foto

Imagen pequeña
++++++++++++++

.. figure::  images/modulo/login.png
   :align:   center
   :width: 30%

   Pie de foto

Imagen incrustada en texto
++++++++++++++++++++++++++

.. |image68| image:: images/modulo/image68.png
       :width: 80

Esto es una imagen incrustada |image68|


Links de documentación
++++++++++++++++++++++

https://pythonhosted.org/an_example_pypi_project/sphinx.html

http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html