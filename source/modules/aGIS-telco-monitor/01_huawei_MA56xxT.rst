##################
aGIS Telco Monitor
##################

aGIS Telco Monitor nos permite la monitorización de las olts mediante una conexión remota y el protocolo SNMP.

.. figure::  images/aGIS-telco-monitor/00_introduccion/image0.png
   :align:  center
   :width: 80%

   Dashboard

Como consecuencia de que realizamos la monitorización de la manera descrita, dependiendo del fabricante de la olt podremos realizar una serie de configuraciones de diferentes maneras.

Huawei MA56xxT
**************

En este capitulo veremos la configuración de monitorización para una olt del fabricante Huawei

Interfaz
========
La interfaz de monitorización de Huawei es bastante sencilla e intuitiva. Se compone de tres vistas que se encuentran en el menú que aparece a la izquierda de la pantalla.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/menu.png
   :align:   center
   :width: 30%

   Menú

Dashboard
=========
Lo primero que encontramos al entrar en la aplicación es el Dashboard, un resumen del estado de los elementos que disponemos en la monitorización.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image1.png
   :align:   center
   :width: 80%

   Dashboard

.. _olts:

Olts config
===========
Olts config nos permite la configuración de olt/slot/port, que deseemos monitorizar. Para ello disponemos de diferentes ventanas a las que podemos acceder mediante los botones, apartado que ampliamos en :ref:`botones`

La pantalla inicial nos muestra una tabla con las diferentes olts que han sido configuradas.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image2.png
   :align:   center
   :width: 80%

   Olts config

Esta tabla contiene datos como: si es posible o no hacer un ping, el nombre de la olt, su localización, su contacto, su descriptor e incluso su tiempo de actividad.

Si accedemos a una olt mediante el botón entrar, nos mostrará sus slots. En caso de querer volver a las olts, pulsaremos en el botón que aparece en la parte superior izquierda.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image3.png
   :align:   center
   :width: 80%

   Slots

Si accedemos a un slot mediante el botón entrar, nos mostrará sus ports. En caso de querer volver a los slots, pulsaremos en el botón que aparece en la parte superior izquierda.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image4.png
   :align:   center
   :width: 80%

   Ports

Desde la ventana de ports, podemos ver el estado en el que se encuentran los distintos ports.



Onus list
=========
Onus list nos muestra en detalle los datos de las ont que se han extraido mediante la configuración de :ref:`olts`

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image5.png
   :align:   center
   :width: 80%

   Onus list

Como vemos en la imagen, podemos ver datos como el estado en el que se encuentran, sus potencias, su numero de serie y la temperatura de cada onu registrada.


.. _botones:

Botones
=======

Vamos a explicar los diferentes botones que encontraremos mientras navegamos por la interfaz:

.. |add| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/add.png
       :width: 20

.. |entrar| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/entrar.png
       :width: 25

.. |ver| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/ver.png
       :width: 25

.. |upd_onu| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/upd_onu.png
       :width: 25

.. |upd_snmp| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/upd_snmp.png
       :width: 25

.. |upd| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/upd.png
       :width: 25

.. |del| image:: images/aGIS-telco-monitor/01_huawei_MA56xxT/del.png
       :width: 25


**Añadir**  |add|
        Mediante el botón añadir, podemos introducir nuevas olt, slots o ports, completando un formulario.

**Entrar**  |entrar|
        Mediante el botón entrar, podemos ir accediendo a cada elemento.

**Ver**  |ver|
        Mediante el boton ver, se muestran los datos que pueden ser configurados desde el formulario de añadir.

**Actualizar onus**  |upd_onu|
        Mediante el botón actualizar onts, se añaden las nuevas onus que fueron introducidas desde la última actualización.

**Actualizar monitorización**  |upd_snmp|
        Mediante el botón "actualizar monitorización", se actualizan los datos que se extraen de las olt y de las onus.

**Editar**  |upd|
        Mediante el botón editar, podemos cambiar los datos del formulario que completamos al añadir.

**Eliminar**  |del|
        Mediante el botón eliminar, podemos borrar cualquier elemento no deseado.

Configuración
=============
Para configurar la monitorización de olts debemos seguir los siguientes pasos en el orden indicado:

1. Olt
------
En primer lugar, debemos añadir una olt completando su formulario de manera correcta.

Para ello iremos a "Olt config" y pulsaremos en el botón añadir. Nos mostrará un formulario, en el cual debemos introducir, como mínimo, los siguientes datos:

General:
    * **ID:** Id de la olt
    * **Name:** Nombre de la olt
    * **IP:** Ip de la olt

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image6.png
   :align:   center
   :width: 90%

SSH:
    * **SSH IP:** Ip de acceso a ssh
    * **SSH user:** Usuario ssh
    * **SSH password:** Contraseña ssh


.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image7.png
   :align:   center
   :width: 90%

SNMP:
    * **SNMP version:** Versión del snmp
    * **SNMP public community:** Comunidad snmp


.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image8.png
   :align:   center
   :width: 90%

2. Slots
--------

Una vez añadida una olt, le damos al botón entrar, que nos llevará a la ventana donde añadiremos los slots que deseemos monitorizar.

Para ello, le damos al boton añadir y rellenamos el formulario

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image9.png
   :align:   center
   :width: 80%

   Formulario Slots


3. Ports
--------
Una vez añadidos los slots, le damos al botón entrar que aparece en la fila del slot al que deseemos añadir un port. Esto nos llevará a la ventana donde añadiremos los ports que deseemos monitorizar.

Para ello, le damos al boton añadir y rellenamos el formulario

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image10.png
   :align:   center
   :width: 80%

   Formulario Ports

4. Actualizar onus |upd_onu|
----------------------------
Una vez hemos configurado los pasos anteriores, podemos actualizar las onus.

Por defecto, se actualizan dos veces al día, pero disponemos de dos botones manuales para su actualización inmediata (puede demorar varios minutos).

El primer botón para la actualización manual lo encontramos en Olts config. En este caso, se encuentra en la columna action, y actualiza las onus de dicha olt únicamente.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image2.png
   :align:   center
   :width: 80%

   Olts config

El segundo botón lo encontramos en Onus list, justo debajo del titulo. En este caso, actualizamos las onus de todas las olts que estén correctamente configuradas en Olts config.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image5.png
   :align:   center
   :width: 80%

   Onus list

5. Actualizar monitorización |upd_snmp|
---------------------------------------
Una vez disponemos de las onus, podemos monitorizarlas.

Por defecto, se actualizan cada 15 minutos, pero disponemos de cuatro botones manuales para su actualización inmediata (puede demorar varios minutos).

El primer botón, para la actualización manual, lo encontramos en Olts config. En este caso, se encuentra en la columna action, y actualiza la monitorización de dicha olt únicamente.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image2.png
   :align:   center
   :width: 80%

   Olts config

El segundo botón, lo encontramos en Olts config > Slots > Ports justo debajo del titulo. En este caso, actualiza el estado en el que se encuentran los puertos de dicho slot.

El tercer botón, lo encontramos en Onus list justo debajo del titulo. En este caso, actualizamos todas las olts que estén correctamente configuradas en Olts config.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image5.png
   :align:   center
   :width: 80%

   Onus list

El último boton lo encontramos tambien en Onus list, en este caso se encuentra en la columna action, y actualiza la monitorización de dicha onu.

.. figure::  images//aGIS-telco-monitor/01_huawei_MA56xxT/image5.png
   :align:   center
   :width: 80%

   Onus list

API
===

Una vez hemos configurado correctamente nuestro servidor aGIS Telco Monitor, podemos consumir los datos que se extraen de la aplicación de manera universal gracias a las API. Dichas API son las que nos proporcionan la interoperabilidad entre aGIS TELCO y aGIS TELCO MONITOR.

A continuación mostraremos las diferentes API que posee la aplicación.

1. ONUS
-------

1.1. LISTAR ONUS
++++++++++++++++

.. code-block:: python

   import requests

   url = "HOST/mod_olt_huawei/api/v1/onu_huawei/"
   payload={'chasis_id': '1',
   'slot_id': '0',
   'port_id': '12',
   'ont_id': '0',
   'sn': 'XXX',
   'state': 'disable',
   'snmp_ptx': '0',
   'snmp_prx': '1',
   'snmp_temp': '2'}
   files=[
   ]
   headers = {
   'Authorization': 'Token TOKEN'
   }

   response = requests.request("GET", url, headers=headers, data=payload, files=files)

   print(response.text)

Response:

.. code-block:: json

   [
    {
        "chasis_id": 0,
        "slot_id": 0,
        "port_id": 0,
        "ont_id": 0,
        "sn": "SERIAL_NUMBER",
        "state": "offline",
        "snmp_ptx": "-",
        "snmp_prx": "-",
        "snmp_temp": "-"
    }
   ]



1.2. OBTENER ONU POR SN
+++++++++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/onu_huawei/SERIAL_NUMBER/"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'SERIAL_NUMBER',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)


Response:

.. code-block:: json

   [
    {
        "chasis_id": 0,
        "slot_id": 0,
        "port_id": 0,
        "ont_id": 15,
        "sn": "SERIAL_NUMBER",
        "state": "online ",
        "snmp_ptx": "7.67",
        "snmp_prx": "-20.17",
        "snmp_temp": "36.0"
    }
   ]

2. OLTS
-------


2.1. LISTAR OLTS
++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/olt_huawei/"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'SERIAL_NUMBER',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)


Response:

.. code-block:: json

   [
    {
        "olt_id": 0,
        "olt_name": "NOMBRE_OLT",
        "olt_ip": "123.123.123.123",
        "olt_coordx": null,
        "olt_coordy": null,
        "olt_coorduso": null,
        "olt_sn": "SERIAL",
        "olt_version": "V2",
        "olt_softversion": "Software version",
        "olt_desc": "A description",
        "olt_timestamp": "2020/11/30 14:46:50",
        "snmp_port": 161,
        "snmp_public_community": "public",
        "snmp_version": "v2c",
        "ssh_ip": "123.123.123.123",
        "ssh_port": null,
        "ssh_user": "USER",
        "ssh_pwd": "PASSWORD",
        "snmp_desc": "Huawei Integrated Access Software",
        "snmp_time": "62 days",
        "snmp_contact": "R&D Shenzhen, Huawei Technologies Co., Ltd.",
        "snmp_name": "NOMBRE",
        "snmp_location": "SITIO",
        "snmp_msgreceived": "61184",
        "snmp_msganswered": "61168",
        "snmp_msgattend": "61169",
        "ping": "NO"
    }
   ]

2.2. OBTENER OLT POR olt_name
+++++++++++++++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/olt_huawei/NAME_OLT"

    payload={}
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    print(response.text)


Response:

.. code-block:: json

   [
    {
        "olt_id": 0,
        "olt_name": "NAME",
        "olt_ip": "123.123.123.123",
        "olt_coordx": null,
        "olt_coordy": null,
        "olt_coorduso": null,
        "olt_sn": "SERIAL",
        "olt_version": "V2",
        "olt_softversion": "Software version",
        "olt_desc": "A description",
        "olt_timestamp": "2020/11/30 14:46:50",
        "snmp_port": 161,
        "snmp_public_community": "public",
        "snmp_version": "v2c",
        "ssh_ip": "123.123.123.123",
        "ssh_port": null,
        "ssh_user": "USER",
        "ssh_pwd": "PASS",
        "snmp_desc": "Huawei Integrated Access Software",
        "snmp_time": "62 days",
        "snmp_contact": "R&D Shenzhen, Huawei Technologies Co., Ltd.",
        "snmp_name": "NAME",
        "snmp_location": "SITIO",
        "snmp_msgreceived": "61351",
        "snmp_msganswered": "61335",
        "snmp_msgattend": "61336",
        "ping": "NO"
    }
   ]

3. SLOTS
--------

3.1. LISTAR SLOTS
+++++++++++++++++

.. code-block:: python

    import requests


    url = "HOST/mod_olt_huawei/api/v1/slot_huawei/"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'SERIAL',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)

Response:

.. code-block:: json

   [
    {
        "olt": 1,
        "pos": 0,
        "name": "SLOT 0"
    }
   ]

3.2. OBTENER SLOT POR PK
++++++++++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/slot_huawei/1"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'XXX',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)

Response:

.. code-block:: json

   [
    {
        "olt": 1,
        "pos": 0,
        "name": "SLOT 0"
    }
   ]

4. PORTS
--------

4.1. LISTAR PORTS
+++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/port_huawei/"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'XXX',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)


Response:

.. code-block:: json

   [
    {
        "slot": 1,
        "pos": 4,
        "name": "PON 4"
    }
   ]

4.2. OBTENER PORT POR PK
++++++++++++++++++++++++

.. code-block:: python

    import requests

    url = "HOST/mod_olt_huawei/api/v1/port_huawei/14"

    payload={'chasis_id': '1',
    'slot_id': '0',
    'port_id': '12',
    'ont_id': '0',
    'sn': 'XXX',
    'state': 'disable',
    'snmp_ptx': '0',
    'snmp_prx': '1',
    'snmp_temp': '2'}
    files=[

    ]
    headers = {
      'Authorization': 'Token TOKEN'
    }

    response = requests.request("GET", url, headers=headers, data=payload, files=files)

    print(response.text)

Response:

.. code-block:: json

   [
    {
        "slot": 1,
        "pos": 0,
        "name": "PON 0"
    }
   ]
