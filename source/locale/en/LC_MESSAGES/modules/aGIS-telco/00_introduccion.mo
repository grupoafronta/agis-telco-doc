��          L               |   Q   }   �   �      �  �  �  �   `  �    O   �  �   �     �  |  �  �   Z   La documentación adjunta describe la interfaz y uso de la plataforma aGIS TELCO. La plataforma se presenta en una interfaz focalizada en un mapa Web, mediante en el que una serie de capas modelan los distintos elementos de una red, permitiendo el dibujo, representación y conexión de los elementos. aGIS TELCO Introducción aGIS TELCO es una plataforma WebGIS, orientada a la gestión y planificación de Redes de Telecomunicaciones, especialmente redes de Fibra Óptica en cualquiera de sus ámbitos de aplicación (FTTx, Fibra Oscura, Capacidad...), aunque también contempla la integración con redes eléctricas, redes de canalizaciones y obra civil, redes de radio, redes de coaxial así como las combinaciones híbridas de todas. aGIS apuesta por un modelo de datos **abierto**, pues todas las capas y documentación generada pueden exportarse de aGIS e importarse y reprocesarse en cualquier software GIS horizontal. Project-Id-Version: aGIS TELCO Documentación 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-03-09 11:01+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 The attached documentation describes the interface and use of the aGIS platform The platform is presented in an interface focused on a Web map, through which a series of layers model the different elements of a network, allowing the drawing, representation and connection of elements. aGIS TELCO Introduction aGIS TELCO is a WebGIS platform, oriented to the management and planning of Telecommunications Networks, especially networks of Fiber Optic in any of its fields of application (FTTx, Fiber Dark, Capacity...), although it also includes integration with networks electricity, pipeline networks and civil works, radio networks, coaxial networks as well as hybrid combinations of all. aGIS is committed to an **open** data model, since all layers and documentation generated can be exported from aGIS and imported and reprocessed in any horizontal GIS software. 