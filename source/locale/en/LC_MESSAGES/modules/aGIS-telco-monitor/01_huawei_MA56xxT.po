# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, Grupo Afronta S.L.
# This file is distributed under the same license as the aGIS TELCO
# Documentación package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: aGIS TELCO Documentación \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-29 12:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.11.0\n"

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:3
msgid "aGIS Telco Monitor"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:5
msgid ""
"aGIS Telco Monitor nos permite la monitorización de las olts mediante una"
" conexión remota y el protocolo SNMP."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:11
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:31
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:38
msgid "Dashboard"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:13
msgid ""
"Como consecuencia de que realizamos la monitorización de la manera "
"descrita, dependiendo del fabricante de la olt podremos realizar una "
"serie de configuraciones de diferentes maneras."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:16
msgid "Huawei MA56xxT"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:18
msgid ""
"En este capitulo veremos la configuración de monitorización para una olt "
"del fabricante Huawei"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:21
msgid "Interfaz"
msgstr "Interface"

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:22
msgid ""
"La interfaz de monitorización de Huawei es bastante sencilla e intuitiva."
" Se compone de tres vistas que se encuentran en el menú que aparece a la "
"izquierda de la pantalla."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:28
msgid "Menú"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:32
msgid ""
"Lo primero que encontramos al entrar en la aplicación es el Dashboard, un"
" resumen del estado de los elementos que disponemos en la monitorización."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:43
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:52
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:215
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:237
msgid "Olts config"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:44
msgid ""
"Olts config nos permite la configuración de olt/slot/port, que deseemos "
"monitorizar. Para ello disponemos de diferentes ventanas a las que "
"podemos acceder mediante los botones, apartado que ampliamos en "
":ref:`botones`"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:46
msgid ""
"La pantalla inicial nos muestra una tabla con las diferentes olts que han"
" sido configuradas."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:54
msgid ""
"Esta tabla contiene datos como: si es posible o no hacer un ping, el "
"nombre de la olt, su localización, su contacto, su descriptor e incluso "
"su tiempo de actividad."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:56
msgid ""
"Si accedemos a una olt mediante el botón entrar, nos mostrará sus slots. "
"En caso de querer volver a las olts, pulsaremos en el botón que aparece "
"en la parte superior izquierda."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:62
msgid "Slots"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:64
msgid ""
"Si accedemos a un slot mediante el botón entrar, nos mostrará sus ports. "
"En caso de querer volver a los slots, pulsaremos en el botón que aparece "
"en la parte superior izquierda."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:70
msgid "Ports"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:72
msgid ""
"Desde la ventana de ports, podemos ver el estado en el que se encuentran "
"los distintos ports."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:77
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:84
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:223
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:247
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:255
msgid "Onus list"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:78
msgid ""
"Onus list nos muestra en detalle los datos de las ont que se han extraido"
" mediante la configuración de :ref:`olts`"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:86
msgid ""
"Como vemos en la imagen, podemos ver datos como el estado en el que se "
"encuentran, sus potencias, su numero de serie y la temperatura de cada "
"onu registrada."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:92
msgid "Botones"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:94
msgid ""
"Vamos a explicar los diferentes botones que encontraremos mientras "
"navegamos por la interfaz:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:96
msgid "add"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:99
msgid "entrar"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:102
msgid "ver"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:105
msgid "upd_onu"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:108
msgid "upd_snmp"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:111
msgid "upd"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:114
msgid "del"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:119
msgid "**Añadir**  |add|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:119
msgid ""
"Mediante el botón añadir, podemos introducir nuevas olt, slots o ports, "
"completando un formulario."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:122
msgid "**Entrar**  |entrar|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:122
msgid "Mediante el botón entrar, podemos ir accediendo a cada elemento."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:125
msgid "**Ver**  |ver|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:125
msgid ""
"Mediante el boton ver, se muestran los datos que pueden ser configurados "
"desde el formulario de añadir."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:128
msgid "**Actualizar onus**  |upd_onu|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:128
msgid ""
"Mediante el botón actualizar onts, se añaden las nuevas onus que fueron "
"introducidas desde la última actualización."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:131
msgid "**Actualizar monitorización**  |upd_snmp|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:131
msgid ""
"Mediante el botón \"actualizar monitorización\", se actualizan los datos "
"que se extraen de las olt y de las onus."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:134
msgid "**Editar**  |upd|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:134
msgid ""
"Mediante el botón editar, podemos cambiar los datos del formulario que "
"completamos al añadir."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:137
msgid "**Eliminar**  |del|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:137
msgid "Mediante el botón eliminar, podemos borrar cualquier elemento no deseado."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:140
msgid "Configuración"
msgstr "Configuration"

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:141
msgid ""
"Para configurar la monitorización de olts debemos seguir los siguientes "
"pasos en el orden indicado:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:144
msgid "1. Olt"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:145
msgid ""
"En primer lugar, debemos añadir una olt completando su formulario de "
"manera correcta."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:147
msgid ""
"Para ello iremos a \"Olt config\" y pulsaremos en el botón añadir. Nos "
"mostrará un formulario, en el cual debemos introducir, como mínimo, los "
"siguientes datos:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:152
msgid "General:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:150
msgid "**ID:** Id de la olt"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:151
msgid "**Name:** Nombre de la olt"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:152
msgid "**IP:** Ip de la olt"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:162
msgid "SSH:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:159
msgid "**SSH IP:** Ip de acceso a ssh"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:160
msgid "**SSH user:** Usuario ssh"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:161
msgid "**SSH password:** Contraseña ssh"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:171
msgid "SNMP:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:169
msgid "**SNMP version:** Versión del snmp"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:170
msgid "**SNMP public community:** Comunidad snmp"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:178
msgid "2. Slots"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:180
msgid ""
"Una vez añadida una olt, le damos al botón entrar, que nos llevará a la "
"ventana donde añadiremos los slots que deseemos monitorizar."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:182
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:195
msgid "Para ello, le damos al boton añadir y rellenamos el formulario"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:188
msgid "Formulario Slots"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:192
msgid "3. Ports"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:193
msgid ""
"Una vez añadidos los slots, le damos al botón entrar que aparece en la "
"fila del slot al que deseemos añadir un port. Esto nos llevará a la "
"ventana donde añadiremos los ports que deseemos monitorizar."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:201
msgid "Formulario Ports"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:204
msgid "4. Actualizar onus |upd_onu|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:205
msgid ""
"Una vez hemos configurado los pasos anteriores, podemos actualizar las "
"onus."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:207
msgid ""
"Por defecto, se actualizan dos veces al día, pero disponemos de dos "
"botones manuales para su actualización inmediata (puede demorar varios "
"minutos)."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:209
msgid ""
"El primer botón para la actualización manual lo encontramos en Olts "
"config. En este caso, se encuentra en la columna action, y actualiza las "
"onus de dicha olt únicamente."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:217
msgid ""
"El segundo botón lo encontramos en Onus list, justo debajo del titulo. En"
" este caso, actualizamos las onus de todas las olts que estén "
"correctamente configuradas en Olts config."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:226
msgid "5. Actualizar monitorización |upd_snmp|"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:227
msgid "Una vez disponemos de las onus, podemos monitorizarlas."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:229
msgid ""
"Por defecto, se actualizan cada 15 minutos, pero disponemos de cuatro "
"botones manuales para su actualización inmediata (puede demorar varios "
"minutos)."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:231
msgid ""
"El primer botón, para la actualización manual, lo encontramos en Olts "
"config. En este caso, se encuentra en la columna action, y actualiza la "
"monitorización de dicha olt únicamente."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:239
msgid ""
"El segundo botón, lo encontramos en Olts config > Slots > Ports justo "
"debajo del titulo. En este caso, actualiza el estado en el que se "
"encuentran los puertos de dicho slot."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:241
msgid ""
"El tercer botón, lo encontramos en Onus list justo debajo del titulo. En "
"este caso, actualizamos todas las olts que estén correctamente "
"configuradas en Olts config."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:249
msgid ""
"El último boton lo encontramos tambien en Onus list, en este caso se "
"encuentra en la columna action, y actualiza la monitorización de dicha "
"onu."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:258
msgid "API"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:260
msgid ""
"Una vez hemos configurado correctamente nuestro servidor aGIS Telco "
"Monitor, podemos consumir los datos que se extraen de la aplicación de "
"manera universal gracias a las API. Dichas API son las que nos "
"proporcionan la interoperabilidad entre aGIS TELCO y aGIS TELCO MONITOR."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:262
msgid "A continuación mostraremos las diferentes API que posee la aplicación."
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:265
msgid "1. ONUS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:268
msgid "1.1. LISTAR ONUS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:294
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:344
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:396
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:451
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:520
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:561
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:606
#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:647
msgid "Response:"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:315
msgid "1.2. OBTENER ONU POR SN"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:363
msgid "2. OLTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:367
msgid "2.1. LISTAR OLTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:433
msgid "2.2. OBTENER OLT POR olt_name"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:488
msgid "3. SLOTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:491
msgid "3.1. LISTAR SLOTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:533
msgid "3.2. OBTENER SLOT POR PK"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:574
msgid "4. PORTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:577
msgid "4.1. LISTAR PORTS"
msgstr ""

#: ../../source/modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst:619
msgid "4.2. OBTENER PORT POR PK"
msgstr ""

#~ msgid ""
#~ "La interfaz de monitorización de Huawei"
#~ " es bastante sencilla e intuitiva. Se"
#~ " compone por tres vistas que se "
#~ "encuentran en el menú que aparece "
#~ "a la izquierda de la pantalla."
#~ msgstr ""

#~ msgid ""
#~ "Lo primero que encontramos al entrar "
#~ "en la aplicación es el Dashboard, "
#~ "un resumen del estado de los "
#~ "elementos que disponemos en la "
#~ "monitorización"
#~ msgstr ""

#~ msgid ""
#~ "Si accedemos a una olt mediante el"
#~ " botón entrar, nos mostrará sus "
#~ "slots. En caso de querer volver a"
#~ " las olts pulsaremos en el botón "
#~ "que aparece en la parte superior "
#~ "izquierda."
#~ msgstr ""

#~ msgid ""
#~ "Si accedemos a un slot mediante el"
#~ " botón entrar, nos mostrará sus "
#~ "ports. En caso de querer volver a"
#~ " los slots pulsaremos en el botón "
#~ "que aparece en la parte superior "
#~ "izquierda."
#~ msgstr ""

#~ msgid ""
#~ "Desde la ventana de ports podemos "
#~ "ver el estado en el que se "
#~ "encuentran los distintos ports."
#~ msgstr ""

#~ msgid ""
#~ "Como vemos en la imagen podemos "
#~ "ver datos como el estado en el "
#~ "que se encuentran, sus potencias, su "
#~ "numero de serie y la temperatura "
#~ "de cada onu registrada."
#~ msgstr ""

#~ msgid ""
#~ "Vamos a explicar los diferentes botones"
#~ " que vamos a encontrar mientras "
#~ "navegamos por la interfaz:"
#~ msgstr ""

#~ msgid ""
#~ "Mediante el botón añadir podemos "
#~ "introducir nuevas olt, slots o ports,"
#~ " completando un formulario."
#~ msgstr ""

#~ msgid "Mediante el botón entrar podemos ir accediendo a cada elemento."
#~ msgstr ""

#~ msgid ""
#~ "Mediante el boton ver se muestran "
#~ "los datos que pueden ser configurados"
#~ " desde el formulario de añadir."
#~ msgstr ""

#~ msgid ""
#~ "Mediante el botón actualizar onts se "
#~ "añaden las nuevas onus que fueron "
#~ "introducidas desde la última actualización."
#~ msgstr ""

#~ msgid ""
#~ "Mediante el botón actualizar monitorización"
#~ " se actualizan los datos que se "
#~ "extraen de las olt y de las "
#~ "onus."
#~ msgstr ""

#~ msgid ""
#~ "Mediante el botón editar podemos cambiar"
#~ " los datos del formulario que "
#~ "completamos al añadir."
#~ msgstr ""

#~ msgid ""
#~ "Mediante el botón eliminar podemos "
#~ "borrar cualquier elemento no deseado."
#~ msgstr ""

#~ msgid ""
#~ "En primer lugar debemos añadir una "
#~ "olt completando su formulario de manera"
#~ " correcta."
#~ msgstr ""

#~ msgid ""
#~ "Para ello iremos a Olt config y"
#~ " pulsaremos en el botón añadir. Nos"
#~ " mostrará un formulario, en el cual"
#~ " debemos introducir como mínimo los "
#~ "siguientes datos:"
#~ msgstr ""

#~ msgid ""
#~ "Una vez añadida una olt de damos"
#~ " al botón entrar, que nos llevará "
#~ "a la ventana donde añadiremos los "
#~ "slots que deseemos monitorizar."
#~ msgstr ""

#~ msgid "Para ello le damos al boton añadir y rellenamos el formulario"
#~ msgstr ""

#~ msgid ""
#~ "Una vez añadidos los slots de "
#~ "damos al botón entrar quea parece "
#~ "en la fila del slot al que "
#~ "deseemos añadir un port. Esto nos "
#~ "llevará a la ventana donde añadiremos"
#~ " los ports que deseemos monitorizar."
#~ msgstr ""

#~ msgid ""
#~ "Por defecto se actualizan dos veces "
#~ "al día, pero disponemos de dos "
#~ "botones manuales para su actualización "
#~ "inmediata (puede demorar varios minutos)."
#~ msgstr ""

#~ msgid ""
#~ "El primer botón para la actualización"
#~ " manual lo encontramos en Olts "
#~ "config, en este caso se encuentra "
#~ "en la columna action, y actualiza "
#~ "las onus de dicha olt únicamente."
#~ msgstr ""

#~ msgid ""
#~ "El segundo botón lo encontramos en "
#~ "Onus list justo debajo del titulo, "
#~ "en este caso actualizamos las onus "
#~ "de todas las olts que estén "
#~ "correctamente configuradas en Olts config."
#~ msgstr ""

#~ msgid "Una vez ya disponemos de las onus, podemos monitorizarlas."
#~ msgstr ""

#~ msgid ""
#~ "Por defecto se actualizan cada 15 "
#~ "minutos, pero disponemos de cuatro "
#~ "botones manuales para su actualización "
#~ "inmediata (puede demorar varios minutos)."
#~ msgstr ""

#~ msgid ""
#~ "El primer botón para la actualización"
#~ " manual lo encontramos en Olts "
#~ "config, en este caso se encuentra "
#~ "en la columna action, y actualiza "
#~ "la monitorización de dicha olt "
#~ "únicamente."
#~ msgstr ""

#~ msgid ""
#~ "El segundo botón lo encontramos en "
#~ "Olts config > Slots > Ports justo"
#~ " debajo del titulo, en este caso "
#~ "actualiza el estado en el que se"
#~ " encuentran los puertos de dicho "
#~ "slot."
#~ msgstr ""

#~ msgid ""
#~ "El tercer botón lo encontramos en "
#~ "Onus list justo debajo del titulo, "
#~ "en este caso actualizamos todas las "
#~ "olts que estén correctamente configuradas "
#~ "en Olts config."
#~ msgstr ""

#~ msgid "En este apartado podemos encotrar las API de la aplicación."
#~ msgstr ""

