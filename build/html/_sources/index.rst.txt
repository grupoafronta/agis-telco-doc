.. aGIS TELCO Doc documentation master file, created by
   sphinx-quickstart on Thu Nov 12 12:28:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la Documentación de aGIS TELCO.
============================================

.. toctree::
   :maxdepth: 2
   :caption: aGIS TELCO:


   modules/aGIS-telco/00_introduccion.rst
   modules/aGIS-telco/01_acceso.rst
   modules/aGIS-telco/02_interfaz.rst
   modules/aGIS-telco/02b_opciones_aGIS.rst
   modules/aGIS-telco/03_funcionalidades.rst
   modules/aGIS-telco/04_logica.rst
   modules/aGIS-telco/05_monitorizacion.rst
   modules/aGIS-telco/06_utilidades.rst
   modules/aGIS-telco/07_agisAcademia.rst
   modules/aGIS-telco/08_opciones_admin.rst



.. toctree::
   :maxdepth: 2
   :caption: aGIS TELCO MONITOR:


   modules/aGIS-telco-monitor/01_huawei_MA56xxT.rst

